﻿Public Class FrmConfiguracao
    Private Sub FrmConfiguracao_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        WindowState = FormWindowState.Maximized
        CARREGAR_INI()

        'Carrega informações do Banco de dados Firebird
        txBancoTeorema.Text = FIREBIRD_BANCO
        txPortaFirebird.Text = FIREBIRD_PORTA
        txSenhaFirebird.Text = FIREBIRD_SENHA
        txServidorFirebird.Text = FIREBIRD_SERVIDOR
        txUsuarioFirebird.Text = FIREBIRD_USUARIO



        'Carrega dados Firebird Conexão 2
        txBancoTeorema2.Text = FIREBIRD_BANCO2
        txPortaFirebird2.Text = FIREBIRD_PORTA2
        txSenhaFirebird2.Text = FIREBIRD_SENHA2
        txServidorFirebird2.Text = FIREBIRD_SERVIDOR2
        txUsuarioFirebird2.Text = FIREBIRD_USUARIO2



    End Sub
    'FECHA A TELA DE CONFIGURAÇÃO
    Private Sub cmdVoltar_Click(sender As Object, e As EventArgs) Handles cmdVoltar.Click
        Me.Close()
    End Sub

    'EVENTOS DO BOTÃO SALVAR
    Private Sub cmdSalvar_Click(sender As Object, e As EventArgs) Handles cmdSalvar.Click

        FIREBIRD_BANCO = txBancoTeorema.Text
        FIREBIRD_PORTA = txPortaFirebird.Text
        FIREBIRD_USUARIO = txUsuarioFirebird.Text
        FIREBIRD_SENHA = txSenhaFirebird.Text
        FIREBIRD_SERVIDOR = txServidorFirebird.Text


        FIREBIRD_BANCO2 = txBancoTeorema2.Text
        FIREBIRD_PORTA2 = txPortaFirebird2.Text
        FIREBIRD_USUARIO2 = txUsuarioFirebird2.Text
        FIREBIRD_SENHA2 = txSenhaFirebird2.Text
        FIREBIRD_SERVIDOR2 = txServidorFirebird2.Text

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        OpenFileDialog1.Filter = "Banco de Firebird (*.FDB)|*.FDB"
        OpenFileDialog1.FileName = ""
        OpenFileDialog1.ShowDialog()
        txBancoTeorema.Text = (OpenFileDialog1.FileName)

    End Sub

    Private Sub cmdTestarFirebird_Click(sender As Object, e As EventArgs) Handles cmdTestarFirebird.Click
        CARREGAR_INI()
        Try
            SQL = "SELECT * FROM EMPRESAS"
            TABELA = "EMPRESAS"
            CONEXAO_FIREBIRD(SQL)
            If fs.Tables(TABELA).Rows.Count > 1 Then
                MsgBox("Conexão Efetuada com Sucesso!")
            End If
        Catch
            MsgBox("Erro de Conexão, Verifique!")
        End Try
    End Sub


    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        OpenFileDialog1.Filter = "Banco de Firebird (*.FDB)|*.FDB"
        OpenFileDialog1.FileName = ""
        OpenFileDialog1.ShowDialog()
        txBancoTeorema2.Text = (OpenFileDialog1.FileName)
    End Sub

    Private Sub btnImportacao_Click(sender As Object, e As EventArgs) Handles btnImportacao.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
            txDiretorioImpressao.Text = FolderBrowserDialog1.SelectedPath + "\"
        End If
    End Sub


End Class