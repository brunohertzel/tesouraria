﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmConfiguracao
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        '
        'FrmConfiguracao
        '
        Me.ClientSize = New System.Drawing.Size(966, 598)
        Me.Name = "FrmConfiguracao"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents pnlBotoes As Panel
    Friend WithEvents cmdEditar As Button
    Friend WithEvents cmdSalvar As Button
    Friend WithEvents cmdVoltar As Button
    Friend WithEvents Groupbox2 As GroupBox
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents cbImpressora As ComboBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents rbPrecoVigente As RadioButton
    Friend WithEvents rbPrecoOferta As RadioButton
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents cmdTestarFirebird As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents txSenhaFirebird As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txUsuarioFirebird As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txPortaFirebird As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txServidorFirebird As TextBox
    Friend WithEvents txBancoTeorema As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents Button4 As Button
    Friend WithEvents Label13 As Label
    Friend WithEvents txSenhaFirebird2 As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents txUsuarioFirebird2 As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents txPortaFirebird2 As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents txServidorFirebird2 As TextBox
    Friend WithEvents txBancoTeorema2 As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents Button5 As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents cbxImpAuto As CheckBox
    Friend WithEvents GroupBox8 As GroupBox
    Friend WithEvents rbImpCompleta As RadioButton
    Friend WithEvents rbImpAlterados As RadioButton
    Friend WithEvents cbHora As ComboBox
    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents GroupBox9 As GroupBox
    Friend WithEvents rbImpBanco As RadioButton
    Friend WithEvents rbImpLocal As RadioButton
    Friend WithEvents GroupBox10 As GroupBox
    Friend WithEvents txDiretorioImpressao As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents btnImportacao As Button
    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
    Friend WithEvents GroupBox11 As GroupBox
    Friend WithEvents chbxAutoImpressaoServico As CheckBox
    Friend WithEvents GroupBox12 As GroupBox
    Friend WithEvents chbEstoque As CheckBox
    Friend WithEvents GroupBox13 As GroupBox
    Friend WithEvents GroupBox14 As GroupBox
    Friend WithEvents rbImpBancoAtacado As RadioButton
    Friend WithEvents rbImpLocalAtacado As RadioButton
    Friend WithEvents Grupo As GroupBox
    Friend WithEvents rbTabela As RadioButton
    Friend WithEvents rbCampanha As RadioButton
    Friend WithEvents GroupBox16 As GroupBox
    Friend WithEvents gboxAtacado As GroupBox
    Friend WithEvents cbTabelaPreco As ComboBox
    Friend WithEvents GroupBox15 As GroupBox
    Friend WithEvents chbEtiquetaAtacado As CheckBox
    Friend WithEvents cbImpressoraAtacado As ComboBox
    Friend WithEvents GroupBox17 As GroupBox
    Friend WithEvents cbhAtacadoAtivo As CheckBox
End Class
