﻿Public Class frmMenuPrincipal

    'Eventos de Abertura do Form
    Private Sub frmMenuPrincipal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CARREGAR_INI()
        abreform(ToolStrip1)
        tlbUsuario.Text = USUARIO_LOGADO_NOME
    End Sub

    'Evento Botão Configurações
    Private Sub ConexõesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConexõesToolStripMenuItem.Click
        frmConexoes.MdiParent = Me
        frmConexoes.Show()
    End Sub

    'Evento do botão de Encerramento da aplicação
    Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs) Handles ToolStripButton1.Click

        If (MessageBox.Show("Deseja realmente sair do Sistema??", "Confirme a saída", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes) Then
            gerarLog = 1
            logDado = "Encerramento do Sistema com usuário " & USUARIO_LOGADO_NOME
            logTipo = "LO"
            GravarLog()
            Application.Exit()
        End If

    End Sub

    'Evento do Botão Cadastro de Cartoes
    Private Sub CartõesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CartõesToolStripMenuItem.Click
        frmCadastroCartoesLista.MdiParent = Me
        frmCadastroCartoesLista.Show()
    End Sub
    Private Sub ToolStripButton3_Click(sender As Object, e As EventArgs) Handles ToolStripButton3.Click
        frmCadastroCartoesLista.MdiParent = Me
        frmCadastroCartoesLista.Show()
    End Sub

    'Evento do Botão Cadastro de Empresas
    Private Sub EmpresasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EmpresasToolStripMenuItem.Click
        frmCadastroEmpresasLista.MdiParent = Me
        frmCadastroEmpresasLista.Show()
    End Sub

    Private Sub btnEmpresas_Click(sender As Object, e As EventArgs) Handles btnEmpresas.Click
        EmpresasToolStripMenuItem_Click(Nothing, Nothing)
    End Sub


    'Cadastro de Usuarios
    Private Sub CadastroDeUsuáriosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CadastroDeUsuáriosToolStripMenuItem.Click
        frmCadastroUsuarioLista.MdiParent = Me
        frmCadastroUsuarioLista.Show()
    End Sub

    'Botão de Logs
    Private Sub AuditoriaLOGsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnLOGs.Click
        frmLOGs.MdiParent = Me
        frmLOGs.Show()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        lbDataHora.Text = (DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"))

        Dim formsFilhos() As Form = Me.MdiChildren
        If formsFilhos.Length.ToString() = 0 Then
            lbFormAtivo.Text = "Menu Principal"
        Else
            lbFormAtivo.Text = ActiveMdiChild.Text
        End If
    End Sub

    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        If My.Computer.Keyboard.CapsLock Then
            lbCaps.ForeColor = Color.Black
        Else
            lbCaps.ForeColor = Color.Gray
        End If

        If My.Computer.Keyboard.NumLock Then
            lbNum.ForeColor = Color.Black
        Else
            lbNum.ForeColor = Color.Gray
        End If

        If My.Computer.Keyboard.ScrollLock Then
            lbScrol.ForeColor = Color.Black
        Else
            lbScrol.ForeColor = Color.Gray
        End If
    End Sub


End Class
