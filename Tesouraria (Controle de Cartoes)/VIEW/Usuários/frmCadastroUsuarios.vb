﻿Imports FirebirdSql.Data.FirebirdClient
Public Class frmCadastroUsuarios

    Dim cripto As New cls_crypto

    'Eventos de Abertura do Form
    Private Sub frmCadastroUsuarios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Normal
        Me.FormBorderStyle = FormBorderStyle.None
        Me.WindowState = FormWindowState.Maximized
        CARREGAR_INI()
        abreform(ToolStrip1)
        ID_INCLUIDO = ""
    End Sub

    'Valida as senhas digitadas se são iguais
    Private Sub txSenha1_TextChanged(sender As Object, e As EventArgs) Handles txSenha1.TextChanged
        If txSenha1.Text <> txSenha2.Text Then
            PictureBox1.Image = My.Resources.Cancel
        Else
            PictureBox1.Image = My.Resources.Ok
        End If
    End Sub

    'Valida as senhas digitadas se são iguais
    Private Sub txSenha2_TextChanged(sender As Object, e As EventArgs) Handles txSenha2.TextChanged
        If txSenha1.Text <> txSenha2.Text Then
            PictureBox1.Image = My.Resources.Cancel
        Else
            PictureBox1.Image = My.Resources.Ok
        End If
    End Sub

    Private Sub btnVoltar_Click(sender As Object, e As EventArgs) Handles btnVoltar.Click
        Me.Close()
    End Sub

    Private Sub btnSalvar_Click(sender As Object, e As EventArgs) Handles btnSalvar.Click
        If txSenha1.Text <> txSenha2.Text Then
            MsgBox("Senhas nao conferem, Verifique novamente!", vbCritical)
            Exit Sub
        End If
        Dim status As String = ""
        If rbAtivo.Checked = True Then
            status = "A"
        ElseIf rbInativo.Checked = True Then
            status = "I"
        End If
        SQL = "INSERT INTO USUARIOS (USUARIO_LOGIN, USUARIO_SENHA, USUARIO_NOME, USUARIO_EMAIL, USUARIO_STATUS) VALUES ("
        SQL += "'" & txLogin.Text & "',"
        SQL += "'" & cripto.Encrypt(txSenha1.Text) & "',"
        SQL += "'" & txNome.Text & "',"
        SQL += "'" & txEmail.Text & "',"
        SQL += "'" & status & "') RETURNING USUARIO_ID;"
        CONEXAO_NOVA(SQL, "USUARIO_ID")
        txUsuarioCod.Text = ID_INCLUIDO
        MsgBox("Usuário Incluído com Sucesso!", vbInformation)
    End Sub

    Private Sub btnIncluir_Click(sender As Object, e As EventArgs) Handles btnIncluir.Click
        Limpar(Me)
    End Sub

End Class