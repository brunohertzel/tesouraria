﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCadastroUsuarioLista
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ExportarTextoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportarHTMLToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportarExcelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExcluirGradeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalvarGradeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnGrade = New System.Windows.Forms.ToolStripSplitButton()
        Me.ExportarPDFToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnEditar = New System.Windows.Forms.ToolStripButton()
        Me.btnIncluir = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnVoltar = New System.Windows.Forms.ToolStripButton()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rbInativo = New System.Windows.Forms.RadioButton()
        Me.rbAtivo = New System.Windows.Forms.RadioButton()
        Me.lbPesquisa = New System.Windows.Forms.Label()
        Me.btnLocalizar = New System.Windows.Forms.Button()
        Me.txBusca = New System.Windows.Forms.TextBox()
        Me.gcUsuarios = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.ToolStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.gcUsuarios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ExportarTextoToolStripMenuItem
        '
        Me.ExportarTextoToolStripMenuItem.Name = "ExportarTextoToolStripMenuItem"
        Me.ExportarTextoToolStripMenuItem.Size = New System.Drawing.Size(153, 22)
        Me.ExportarTextoToolStripMenuItem.Text = "Exportar Texto"
        '
        'ExportarHTMLToolStripMenuItem
        '
        Me.ExportarHTMLToolStripMenuItem.Name = "ExportarHTMLToolStripMenuItem"
        Me.ExportarHTMLToolStripMenuItem.Size = New System.Drawing.Size(153, 22)
        Me.ExportarHTMLToolStripMenuItem.Text = "Exportar HTML"
        '
        'ExportarExcelToolStripMenuItem
        '
        Me.ExportarExcelToolStripMenuItem.Name = "ExportarExcelToolStripMenuItem"
        Me.ExportarExcelToolStripMenuItem.Size = New System.Drawing.Size(153, 22)
        Me.ExportarExcelToolStripMenuItem.Text = "Exportar Excel"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(150, 6)
        '
        'ExcluirGradeToolStripMenuItem
        '
        Me.ExcluirGradeToolStripMenuItem.Name = "ExcluirGradeToolStripMenuItem"
        Me.ExcluirGradeToolStripMenuItem.Size = New System.Drawing.Size(153, 22)
        Me.ExcluirGradeToolStripMenuItem.Text = "Excluir Grade"
        '
        'SalvarGradeToolStripMenuItem
        '
        Me.SalvarGradeToolStripMenuItem.Name = "SalvarGradeToolStripMenuItem"
        Me.SalvarGradeToolStripMenuItem.Size = New System.Drawing.Size(153, 22)
        Me.SalvarGradeToolStripMenuItem.Text = "Salvar Grade"
        '
        'btnGrade
        '
        Me.btnGrade.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SalvarGradeToolStripMenuItem, Me.ExcluirGradeToolStripMenuItem, Me.ToolStripSeparator2, Me.ExportarExcelToolStripMenuItem, Me.ExportarHTMLToolStripMenuItem, Me.ExportarPDFToolStripMenuItem, Me.ExportarTextoToolStripMenuItem})
        Me.btnGrade.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.salva_grade48
        Me.btnGrade.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnGrade.Name = "btnGrade"
        Me.btnGrade.Size = New System.Drawing.Size(54, 53)
        Me.btnGrade.Text = "Grade"
        Me.btnGrade.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ExportarPDFToolStripMenuItem
        '
        Me.ExportarPDFToolStripMenuItem.Name = "ExportarPDFToolStripMenuItem"
        Me.ExportarPDFToolStripMenuItem.Size = New System.Drawing.Size(153, 22)
        Me.ExportarPDFToolStripMenuItem.Text = "Exportar PDF"
        '
        'btnEditar
        '
        Me.btnEditar.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Edit_page
        Me.btnEditar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(41, 53)
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnIncluir
        '
        Me.btnIncluir.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources._1489198437_Add
        Me.btnIncluir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnIncluir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnIncluir.Name = "btnIncluir"
        Me.btnIncluir.Size = New System.Drawing.Size(44, 53)
        Me.btnIncluir.Text = "&Incluir"
        Me.btnIncluir.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnIncluir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 56)
        '
        'btnVoltar
        '
        Me.btnVoltar.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Log_Out_Icon_32
        Me.btnVoltar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnVoltar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnVoltar.Name = "btnVoltar"
        Me.btnVoltar.Size = New System.Drawing.Size(41, 53)
        Me.btnVoltar.Text = "&Voltar"
        Me.btnVoltar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnVoltar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.Color.SteelBlue
        Me.ToolStrip1.GripMargin = New System.Windows.Forms.Padding(3, 3, 4, 3)
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(34, 34)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnVoltar, Me.ToolStripSeparator1, Me.btnIncluir, Me.btnEditar, Me.btnGrade})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Margin = New System.Windows.Forms.Padding(4)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1298, 56)
        Me.ToolStrip1.TabIndex = 6
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.lbPesquisa)
        Me.GroupBox1.Controls.Add(Me.btnLocalizar)
        Me.GroupBox1.Controls.Add(Me.txBusca)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Location = New System.Drawing.Point(0, 56)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1298, 71)
        Me.GroupBox1.TabIndex = 9
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Cadastro de Usuários"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rbInativo)
        Me.GroupBox2.Controls.Add(Me.rbAtivo)
        Me.GroupBox2.Location = New System.Drawing.Point(451, 7)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(136, 58)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Status"
        '
        'rbInativo
        '
        Me.rbInativo.AutoSize = True
        Me.rbInativo.Location = New System.Drawing.Point(66, 24)
        Me.rbInativo.Name = "rbInativo"
        Me.rbInativo.Size = New System.Drawing.Size(62, 17)
        Me.rbInativo.TabIndex = 1
        Me.rbInativo.Text = "Inativos"
        Me.rbInativo.UseVisualStyleBackColor = True
        '
        'rbAtivo
        '
        Me.rbAtivo.AutoSize = True
        Me.rbAtivo.Checked = True
        Me.rbAtivo.Location = New System.Drawing.Point(6, 24)
        Me.rbAtivo.Name = "rbAtivo"
        Me.rbAtivo.Size = New System.Drawing.Size(54, 17)
        Me.rbAtivo.TabIndex = 0
        Me.rbAtivo.TabStop = True
        Me.rbAtivo.Text = "Ativos"
        Me.rbAtivo.UseVisualStyleBackColor = True
        '
        'lbPesquisa
        '
        Me.lbPesquisa.AutoSize = True
        Me.lbPesquisa.ForeColor = System.Drawing.Color.DimGray
        Me.lbPesquisa.Location = New System.Drawing.Point(6, 16)
        Me.lbPesquisa.Name = "lbPesquisa"
        Me.lbPesquisa.Size = New System.Drawing.Size(238, 13)
        Me.lbPesquisa.TabIndex = 2
        Me.lbPesquisa.Text = "Digite o Código ou a descrição a ser pesquisado."
        '
        'btnLocalizar
        '
        Me.btnLocalizar.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Search_icon
        Me.btnLocalizar.Location = New System.Drawing.Point(372, 31)
        Me.btnLocalizar.Name = "btnLocalizar"
        Me.btnLocalizar.Size = New System.Drawing.Size(44, 31)
        Me.btnLocalizar.TabIndex = 1
        Me.btnLocalizar.UseVisualStyleBackColor = True
        '
        'txBusca
        '
        Me.txBusca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txBusca.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txBusca.Location = New System.Drawing.Point(6, 31)
        Me.txBusca.Name = "txBusca"
        Me.txBusca.Size = New System.Drawing.Size(360, 31)
        Me.txBusca.TabIndex = 0
        '
        'gcUsuarios
        '
        Me.gcUsuarios.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcUsuarios.Location = New System.Drawing.Point(0, 127)
        Me.gcUsuarios.MainView = Me.GridView1
        Me.gcUsuarios.Name = "gcUsuarios"
        Me.gcUsuarios.Size = New System.Drawing.Size(1298, 551)
        Me.gcUsuarios.TabIndex = 10
        Me.gcUsuarios.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.gcUsuarios
        Me.GridView1.Name = "GridView1"
        '
        'frmCadastroUsuarioLista
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1298, 678)
        Me.Controls.Add(Me.gcUsuarios)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Name = "frmCadastroUsuarioLista"
        Me.Text = "Cadastro de Usuários"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.gcUsuarios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ExportarTextoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExportarHTMLToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExportarExcelToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents ExcluirGradeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SalvarGradeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents btnGrade As ToolStripSplitButton
    Friend WithEvents ExportarPDFToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents btnEditar As ToolStripButton
    Friend WithEvents btnIncluir As ToolStripButton
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents btnVoltar As ToolStripButton
    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents rbInativo As RadioButton
    Friend WithEvents rbAtivo As RadioButton
    Friend WithEvents lbPesquisa As Label
    Friend WithEvents btnLocalizar As Button
    Friend WithEvents txBusca As TextBox
    Friend WithEvents gcUsuarios As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
End Class
