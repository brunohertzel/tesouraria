﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmCadastroUsuarios
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.rbInativo = New System.Windows.Forms.RadioButton()
        Me.rbAtivo = New System.Windows.Forms.RadioButton()
        Me.txUsuarioCod = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txSenha1 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txNome = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txEmail = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txSenha2 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txLogin = New System.Windows.Forms.TextBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btnVoltar = New System.Windows.Forms.ToolStripButton()
        Me.btnIncluir = New System.Windows.Forms.ToolStripButton()
        Me.btnEditar = New System.Windows.Forms.ToolStripButton()
        Me.btnSalvar = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.SystemColors.ControlDark
        Me.ToolStrip1.GripMargin = New System.Windows.Forms.Padding(3, 3, 4, 3)
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(34, 34)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnVoltar, Me.ToolStripSeparator1, Me.btnIncluir, Me.btnEditar, Me.btnSalvar, Me.ToolStripButton2})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Margin = New System.Windows.Forms.Padding(4)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1210, 56)
        Me.ToolStrip1.TabIndex = 5
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 56)
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.PictureBox1)
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.txUsuarioCod)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txSenha1)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txNome)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.txEmail)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txSenha2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txLogin)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Location = New System.Drawing.Point(0, 56)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1210, 243)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Cadastro de Usuários"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.rbInativo)
        Me.GroupBox3.Controls.Add(Me.rbAtivo)
        Me.GroupBox3.Location = New System.Drawing.Point(265, 18)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(119, 43)
        Me.GroupBox3.TabIndex = 31
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Status"
        '
        'rbInativo
        '
        Me.rbInativo.AutoSize = True
        Me.rbInativo.Location = New System.Drawing.Point(61, 19)
        Me.rbInativo.Name = "rbInativo"
        Me.rbInativo.Size = New System.Drawing.Size(57, 17)
        Me.rbInativo.TabIndex = 1
        Me.rbInativo.Text = "Inativo"
        Me.rbInativo.UseVisualStyleBackColor = True
        '
        'rbAtivo
        '
        Me.rbAtivo.AutoSize = True
        Me.rbAtivo.Checked = True
        Me.rbAtivo.Location = New System.Drawing.Point(6, 19)
        Me.rbAtivo.Name = "rbAtivo"
        Me.rbAtivo.Size = New System.Drawing.Size(49, 17)
        Me.rbAtivo.TabIndex = 0
        Me.rbAtivo.TabStop = True
        Me.rbAtivo.Text = "Ativo"
        Me.rbAtivo.UseVisualStyleBackColor = True
        '
        'txUsuarioCod
        '
        Me.txUsuarioCod.Enabled = False
        Me.txUsuarioCod.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txUsuarioCod.Location = New System.Drawing.Point(12, 35)
        Me.txUsuarioCod.Name = "txUsuarioCod"
        Me.txUsuarioCod.ReadOnly = True
        Me.txUsuarioCod.Size = New System.Drawing.Size(69, 26)
        Me.txUsuarioCod.TabIndex = 22
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(87, 20)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(33, 13)
        Me.Label11.TabIndex = 21
        Me.Label11.Text = "Login"
        '
        'txSenha1
        '
        Me.txSenha1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txSenha1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txSenha1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txSenha1.Location = New System.Drawing.Point(12, 81)
        Me.txSenha1.Name = "txSenha1"
        Me.txSenha1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txSenha1.Size = New System.Drawing.Size(233, 26)
        Me.txSenha1.TabIndex = 20
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(12, 110)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(35, 13)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "Nome"
        '
        'txNome
        '
        Me.txNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txNome.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txNome.Location = New System.Drawing.Point(12, 126)
        Me.txNome.Name = "txNome"
        Me.txNome.Size = New System.Drawing.Size(486, 26)
        Me.txNome.TabIndex = 18
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(14, 155)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(34, 13)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "e-mail"
        '
        'txEmail
        '
        Me.txEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txEmail.Location = New System.Drawing.Point(12, 171)
        Me.txEmail.Name = "txEmail"
        Me.txEmail.Size = New System.Drawing.Size(486, 26)
        Me.txEmail.TabIndex = 16
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(262, 65)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(81, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Repita a Senha"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 65)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Senha"
        '
        'txSenha2
        '
        Me.txSenha2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txSenha2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txSenha2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txSenha2.Location = New System.Drawing.Point(265, 81)
        Me.txSenha2.Name = "txSenha2"
        Me.txSenha2.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txSenha2.Size = New System.Drawing.Size(233, 26)
        Me.txSenha2.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Código"
        '
        'txLogin
        '
        Me.txLogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txLogin.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txLogin.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txLogin.Location = New System.Drawing.Point(87, 36)
        Me.txLogin.Name = "txLogin"
        Me.txLogin.Size = New System.Drawing.Size(158, 26)
        Me.txLogin.TabIndex = 0
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(504, 81)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(27, 26)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 32
        Me.PictureBox1.TabStop = False
        '
        'btnVoltar
        '
        Me.btnVoltar.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Log_Out_Icon_32
        Me.btnVoltar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnVoltar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnVoltar.Name = "btnVoltar"
        Me.btnVoltar.Size = New System.Drawing.Size(41, 53)
        Me.btnVoltar.Text = "&Voltar"
        Me.btnVoltar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnVoltar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnIncluir
        '
        Me.btnIncluir.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources._1489198437_Add
        Me.btnIncluir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnIncluir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnIncluir.Name = "btnIncluir"
        Me.btnIncluir.Size = New System.Drawing.Size(44, 53)
        Me.btnIncluir.Text = "&Incluir"
        Me.btnIncluir.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnIncluir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnEditar
        '
        Me.btnEditar.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Edit_page
        Me.btnEditar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(41, 53)
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnSalvar
        '
        Me.btnSalvar.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Apply
        Me.btnSalvar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSalvar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSalvar.Name = "btnSalvar"
        Me.btnSalvar.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.btnSalvar.Size = New System.Drawing.Size(42, 53)
        Me.btnSalvar.Text = "&Salvar"
        Me.btnSalvar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Delete
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(57, 53)
        Me.ToolStripButton2.Text = "&Cancelar"
        Me.ToolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'frmCadastroUsuarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1210, 694)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Name = "frmCadastroUsuarios"
        Me.Text = "Cadastro de Usuários"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents btnVoltar As ToolStripButton
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents btnIncluir As ToolStripButton
    Friend WithEvents btnEditar As ToolStripButton
    Friend WithEvents btnSalvar As ToolStripButton
    Friend WithEvents ToolStripButton2 As ToolStripButton
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txSenha1 As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txNome As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txEmail As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txSenha2 As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txLogin As TextBox
    Friend WithEvents txUsuarioCod As TextBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents rbInativo As RadioButton
    Friend WithEvents rbAtivo As RadioButton
End Class
