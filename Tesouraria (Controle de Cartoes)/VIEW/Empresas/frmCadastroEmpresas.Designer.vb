﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCadastroEmpresas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.rbInativo = New System.Windows.Forms.RadioButton()
        Me.rbAtivo = New System.Windows.Forms.RadioButton()
        Me.txtimg = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txUF = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txCidade = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txCEP = New System.Windows.Forms.MaskedTextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txEmpresaCod = New System.Windows.Forms.TextBox()
        Me.txBairro = New System.Windows.Forms.TextBox()
        Me.txNumero = New System.Windows.Forms.TextBox()
        Me.txInscricao = New System.Windows.Forms.TextBox()
        Me.txComplemento = New System.Windows.Forms.TextBox()
        Me.txLogradouro = New System.Windows.Forms.TextBox()
        Me.txNomeFantasia = New System.Windows.Forms.TextBox()
        Me.txRazaoSocial = New System.Windows.Forms.TextBox()
        Me.txCNPJ = New System.Windows.Forms.MaskedTextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txEmail = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txTelefone = New System.Windows.Forms.TextBox()
        Me.btnApagarImagem = New System.Windows.Forms.Button()
        Me.btnBuscarImagem = New System.Windows.Forms.Button()
        Me.picImagem = New System.Windows.Forms.PictureBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnBuscaReceita = New System.Windows.Forms.Button()
        Me.btnVoltar = New System.Windows.Forms.ToolStripButton()
        Me.btnIncluir = New System.Windows.Forms.ToolStripButton()
        Me.btnEditar = New System.Windows.Forms.ToolStripButton()
        Me.btnSalvar = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.picImagem, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.SystemColors.ControlDark
        Me.ToolStrip1.GripMargin = New System.Windows.Forms.Padding(3, 3, 4, 3)
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(34, 34)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnVoltar, Me.ToolStripSeparator1, Me.btnIncluir, Me.btnEditar, Me.btnSalvar, Me.ToolStripButton2})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Margin = New System.Windows.Forms.Padding(4)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1120, 56)
        Me.ToolStrip1.TabIndex = 4
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 56)
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.txtimg)
        Me.GroupBox1.Controls.Add(Me.btnApagarImagem)
        Me.GroupBox1.Controls.Add(Me.btnBuscarImagem)
        Me.GroupBox1.Controls.Add(Me.picImagem)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.txUF)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txCidade)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.txCEP)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txEmpresaCod)
        Me.GroupBox1.Controls.Add(Me.txBairro)
        Me.GroupBox1.Controls.Add(Me.txNumero)
        Me.GroupBox1.Controls.Add(Me.txInscricao)
        Me.GroupBox1.Controls.Add(Me.txComplemento)
        Me.GroupBox1.Controls.Add(Me.txLogradouro)
        Me.GroupBox1.Controls.Add(Me.txNomeFantasia)
        Me.GroupBox1.Controls.Add(Me.txRazaoSocial)
        Me.GroupBox1.Controls.Add(Me.btnBuscaReceita)
        Me.GroupBox1.Controls.Add(Me.txCNPJ)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Location = New System.Drawing.Point(0, 56)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1120, 263)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Dados da Empresa"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.rbInativo)
        Me.GroupBox3.Controls.Add(Me.rbAtivo)
        Me.GroupBox3.Location = New System.Drawing.Point(480, 19)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(119, 43)
        Me.GroupBox3.TabIndex = 30
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Status"
        '
        'rbInativo
        '
        Me.rbInativo.AutoSize = True
        Me.rbInativo.Location = New System.Drawing.Point(61, 19)
        Me.rbInativo.Name = "rbInativo"
        Me.rbInativo.Size = New System.Drawing.Size(57, 17)
        Me.rbInativo.TabIndex = 1
        Me.rbInativo.Text = "Inativo"
        Me.rbInativo.UseVisualStyleBackColor = True
        '
        'rbAtivo
        '
        Me.rbAtivo.AutoSize = True
        Me.rbAtivo.Checked = True
        Me.rbAtivo.Location = New System.Drawing.Point(6, 19)
        Me.rbAtivo.Name = "rbAtivo"
        Me.rbAtivo.Size = New System.Drawing.Size(49, 17)
        Me.rbAtivo.TabIndex = 0
        Me.rbAtivo.TabStop = True
        Me.rbAtivo.Text = "Ativo"
        Me.rbAtivo.UseVisualStyleBackColor = True
        '
        'txtimg
        '
        Me.txtimg.Location = New System.Drawing.Point(711, 130)
        Me.txtimg.Name = "txtimg"
        Me.txtimg.ReadOnly = True
        Me.txtimg.Size = New System.Drawing.Size(33, 20)
        Me.txtimg.TabIndex = 29
        Me.txtimg.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(655, 203)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(21, 13)
        Me.Label12.TabIndex = 25
        Me.Label12.Text = "UF"
        '
        'txUF
        '
        Me.txUF.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txUF.Location = New System.Drawing.Point(658, 219)
        Me.txUF.Name = "txUF"
        Me.txUF.Size = New System.Drawing.Size(47, 26)
        Me.txUF.TabIndex = 24
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(366, 203)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(40, 13)
        Me.Label11.TabIndex = 23
        Me.Label11.Text = "Cidade"
        '
        'txCidade
        '
        Me.txCidade.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txCidade.Location = New System.Drawing.Point(369, 219)
        Me.txCidade.Name = "txCidade"
        Me.txCidade.Size = New System.Drawing.Size(274, 26)
        Me.txCidade.TabIndex = 22
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(147, 206)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(34, 13)
        Me.Label10.TabIndex = 21
        Me.Label10.Text = "Bairro"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(15, 206)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(28, 13)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "CEP"
        '
        'txCEP
        '
        Me.txCEP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txCEP.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txCEP.Location = New System.Drawing.Point(15, 220)
        Me.txCEP.Mask = "00000-000"
        Me.txCEP.Name = "txCEP"
        Me.txCEP.Size = New System.Drawing.Size(87, 26)
        Me.txCEP.TabIndex = 18
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(456, 154)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(71, 13)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Complemento"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(366, 154)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(44, 13)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Número"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(15, 155)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 13)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Logradouro"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 113)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(78, 13)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Nome Fantasia"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 68)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(70, 13)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Razão Social"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(293, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 13)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Inscrição Estadual"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(89, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(34, 13)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "CNPJ"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Código"
        '
        'txEmpresaCod
        '
        Me.txEmpresaCod.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txEmpresaCod.Location = New System.Drawing.Point(12, 35)
        Me.txEmpresaCod.Name = "txEmpresaCod"
        Me.txEmpresaCod.ReadOnly = True
        Me.txEmpresaCod.Size = New System.Drawing.Size(69, 26)
        Me.txEmpresaCod.TabIndex = 9
        '
        'txBairro
        '
        Me.txBairro.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txBairro.Location = New System.Drawing.Point(150, 220)
        Me.txBairro.Name = "txBairro"
        Me.txBairro.Size = New System.Drawing.Size(213, 26)
        Me.txBairro.TabIndex = 8
        '
        'txNumero
        '
        Me.txNumero.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txNumero.Location = New System.Drawing.Point(369, 170)
        Me.txNumero.Name = "txNumero"
        Me.txNumero.Size = New System.Drawing.Size(84, 26)
        Me.txNumero.TabIndex = 7
        '
        'txInscricao
        '
        Me.txInscricao.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txInscricao.Location = New System.Drawing.Point(296, 36)
        Me.txInscricao.Name = "txInscricao"
        Me.txInscricao.Size = New System.Drawing.Size(178, 26)
        Me.txInscricao.TabIndex = 6
        '
        'txComplemento
        '
        Me.txComplemento.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txComplemento.Location = New System.Drawing.Point(459, 170)
        Me.txComplemento.Name = "txComplemento"
        Me.txComplemento.Size = New System.Drawing.Size(246, 26)
        Me.txComplemento.TabIndex = 5
        '
        'txLogradouro
        '
        Me.txLogradouro.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txLogradouro.Location = New System.Drawing.Point(15, 171)
        Me.txLogradouro.Name = "txLogradouro"
        Me.txLogradouro.Size = New System.Drawing.Size(348, 26)
        Me.txLogradouro.TabIndex = 4
        '
        'txNomeFantasia
        '
        Me.txNomeFantasia.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txNomeFantasia.Location = New System.Drawing.Point(12, 126)
        Me.txNomeFantasia.Name = "txNomeFantasia"
        Me.txNomeFantasia.Size = New System.Drawing.Size(587, 26)
        Me.txNomeFantasia.TabIndex = 3
        '
        'txRazaoSocial
        '
        Me.txRazaoSocial.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txRazaoSocial.Location = New System.Drawing.Point(12, 84)
        Me.txRazaoSocial.Name = "txRazaoSocial"
        Me.txRazaoSocial.Size = New System.Drawing.Size(587, 26)
        Me.txRazaoSocial.TabIndex = 2
        '
        'txCNPJ
        '
        Me.txCNPJ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txCNPJ.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txCNPJ.Location = New System.Drawing.Point(92, 35)
        Me.txCNPJ.Mask = "00,000,000/0000-00"
        Me.txCNPJ.Name = "txCNPJ"
        Me.txCNPJ.Size = New System.Drawing.Size(156, 26)
        Me.txCNPJ.TabIndex = 0
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.txEmail)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.txTelefone)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox2.Location = New System.Drawing.Point(0, 319)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1120, 122)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Dados Contato"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(198, 24)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(32, 13)
        Me.Label14.TabIndex = 29
        Me.Label14.Text = "Email"
        '
        'txEmail
        '
        Me.txEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txEmail.Location = New System.Drawing.Point(198, 40)
        Me.txEmail.Name = "txEmail"
        Me.txEmail.Size = New System.Drawing.Size(507, 26)
        Me.txEmail.TabIndex = 28
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(12, 24)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(49, 13)
        Me.Label13.TabIndex = 27
        Me.Label13.Text = "Telefone"
        '
        'txTelefone
        '
        Me.txTelefone.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txTelefone.Location = New System.Drawing.Point(12, 40)
        Me.txTelefone.Name = "txTelefone"
        Me.txTelefone.Size = New System.Drawing.Size(166, 26)
        Me.txTelefone.TabIndex = 26
        '
        'btnApagarImagem
        '
        Me.btnApagarImagem.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.if_edit_trash_9263
        Me.btnApagarImagem.Location = New System.Drawing.Point(658, 126)
        Me.btnApagarImagem.Name = "btnApagarImagem"
        Me.btnApagarImagem.Size = New System.Drawing.Size(47, 26)
        Me.btnApagarImagem.TabIndex = 28
        Me.btnApagarImagem.UseVisualStyleBackColor = True
        '
        'btnBuscarImagem
        '
        Me.btnBuscarImagem.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.abrir_16x16
        Me.btnBuscarImagem.Location = New System.Drawing.Point(605, 126)
        Me.btnBuscarImagem.Name = "btnBuscarImagem"
        Me.btnBuscarImagem.Size = New System.Drawing.Size(47, 26)
        Me.btnBuscarImagem.TabIndex = 27
        Me.btnBuscarImagem.UseVisualStyleBackColor = True
        '
        'picImagem
        '
        Me.picImagem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picImagem.Location = New System.Drawing.Point(605, 35)
        Me.picImagem.Name = "picImagem"
        Me.picImagem.Size = New System.Drawing.Size(100, 88)
        Me.picImagem.TabIndex = 26
        Me.picImagem.TabStop = False
        '
        'Button1
        '
        Me.Button1.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.correios_logo_2
        Me.Button1.Location = New System.Drawing.Point(108, 220)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(36, 26)
        Me.Button1.TabIndex = 19
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnBuscaReceita
        '
        Me.btnBuscaReceita.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Apps_internet_web_browser_icon
        Me.btnBuscaReceita.Location = New System.Drawing.Point(254, 36)
        Me.btnBuscaReceita.Name = "btnBuscaReceita"
        Me.btnBuscaReceita.Size = New System.Drawing.Size(36, 26)
        Me.btnBuscaReceita.TabIndex = 1
        Me.btnBuscaReceita.UseVisualStyleBackColor = True
        '
        'btnVoltar
        '
        Me.btnVoltar.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Log_Out_Icon_32
        Me.btnVoltar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnVoltar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnVoltar.Name = "btnVoltar"
        Me.btnVoltar.Size = New System.Drawing.Size(41, 53)
        Me.btnVoltar.Text = "&Voltar"
        Me.btnVoltar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnVoltar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnIncluir
        '
        Me.btnIncluir.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources._1489198437_Add
        Me.btnIncluir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnIncluir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnIncluir.Name = "btnIncluir"
        Me.btnIncluir.Size = New System.Drawing.Size(44, 53)
        Me.btnIncluir.Text = "&Incluir"
        Me.btnIncluir.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnIncluir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnEditar
        '
        Me.btnEditar.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Edit_page
        Me.btnEditar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(41, 53)
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnSalvar
        '
        Me.btnSalvar.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Apply
        Me.btnSalvar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSalvar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSalvar.Name = "btnSalvar"
        Me.btnSalvar.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.btnSalvar.Size = New System.Drawing.Size(42, 53)
        Me.btnSalvar.Text = "&Salvar"
        Me.btnSalvar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Delete
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(57, 53)
        Me.ToolStripButton2.Text = "&Cancelar"
        Me.ToolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'frmCadastroEmpresas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1120, 619)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Name = "frmCadastroEmpresas"
        Me.Text = "frmCadastroEmpresas"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.picImagem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents btnVoltar As ToolStripButton
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents btnIncluir As ToolStripButton
    Friend WithEvents btnEditar As ToolStripButton
    Friend WithEvents btnSalvar As ToolStripButton
    Friend WithEvents ToolStripButton2 As ToolStripButton
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents btnBuscaReceita As Button
    Friend WithEvents txCNPJ As MaskedTextBox
    Friend WithEvents txRazaoSocial As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents txUF As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txCidade As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents txCEP As MaskedTextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txEmpresaCod As TextBox
    Friend WithEvents txBairro As TextBox
    Friend WithEvents txNumero As TextBox
    Friend WithEvents txInscricao As TextBox
    Friend WithEvents txComplemento As TextBox
    Friend WithEvents txLogradouro As TextBox
    Friend WithEvents txNomeFantasia As TextBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label14 As Label
    Friend WithEvents txEmail As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents txTelefone As TextBox
    Friend WithEvents btnBuscarImagem As Button
    Friend WithEvents picImagem As PictureBox
    Friend WithEvents btnApagarImagem As Button
    Friend WithEvents txtimg As TextBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents rbInativo As RadioButton
    Friend WithEvents rbAtivo As RadioButton
End Class
