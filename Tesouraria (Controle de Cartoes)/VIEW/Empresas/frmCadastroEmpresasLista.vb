﻿Public Class frmCadastroEmpresasLista

    'Eventos de Abertura do Form
    Private Sub frmCadastroEmpresasLista_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.FormBorderStyle = FormBorderStyle.None
        Me.WindowState = FormWindowState.Normal
        Me.WindowState = FormWindowState.Maximized
        carregarGrid(gcEmpresas, "GridEmpresas")
        CARREGAR_INI()
        abreform(ToolStrip1)
    End Sub

    'Evento do Botão Fechar
    Private Sub btnVoltar_Click(sender As Object, e As EventArgs) Handles btnVoltar.Click
        Me.Close()
    End Sub

    'Evento do Botão de Busca
    Private Sub btnLocalizar_Click(sender As Object, e As EventArgs) Handles btnLocalizar.Click
        Dim STATUS As String = ""
        If rbAtivo.Checked = True Then
            STATUS = "A"
        ElseIf rbInativo.Checked = True Then
            STATUS = "I"
        End If
        SQL = "SELECT * FROM EMPRESAS WHERE EMPRESA_ID LIKE '%" & txBusca.Text & "%' OR EMPRESA_NOME LIKE '%" & txBusca.Text & "%' OR EMPRESA_CNPJ LIKE '%" & txBusca.Text & "%' OR EMPRESA_NOME_FANTASIA LIKE '%" & txBusca.Text & "' AND EMPRESA_STATUS='" & STATUS & "'"
        TABELA = "TABELA"
        CONEXAO_FIREBIRD(SQL)
        gcEmpresas.DataSource = fs.Tables(TABELA)
    End Sub

    'Evento do Botão Incluir
    Private Sub btnIncluir_Click(sender As Object, e As EventArgs) Handles btnIncluir.Click
        frmCadastroEmpresas.MdiParent = frmMenuPrincipal
        frmCadastroEmpresas.Show()
        ENTRADA_DADOS = "I"
    End Sub

    Private Sub gcEmpresas_DoubleClick(sender As Object, e As EventArgs) Handles gcEmpresas.DoubleClick
        EditarEmpresas()
    End Sub

    Sub EditarEmpresas()
        ENTRADA_DADOS = "E"
        If GridView1.RowCount = 0 Then
            MsgBox("Nenhum dado informado para edição!", vbCritical)
            Exit Sub
        Else
            Dim ID As String = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), gEmpresaCodigo)
            SQL = "SELECT * FROM EMPRESAS WHERE EMPRESA_ID=" & ID
            TABELA = "TABELA"
            CONEXAO_FIREBIRD(SQL)
            If fs.Tables(TABELA).Rows.Count > 0 Then
                With frmCadastroEmpresas
                    .txEmpresaCod.Text = fs.Tables(TABELA).Rows(0)("EMPRESA_ID").ToString
                    .txRazaoSocial.Text = fs.Tables(TABELA).Rows(0)("EMPRESA_NOME").ToString
                    .txCNPJ.Text = fs.Tables(TABELA).Rows(0)("EMPRESA_CNPJ").ToString
                    .txInscricao.Text = fs.Tables(TABELA).Rows(0)("EMPRESA_INSCRICAO").ToString
                    .txNomeFantasia.Text = fs.Tables(TABELA).Rows(0)("EMPRESA_NOME_FANTASIA").ToString
                    .txLogradouro.Text = fs.Tables(TABELA).Rows(0)("EMPRESA_LOGRADOURO").ToString
                    .txNumero.Text = fs.Tables(TABELA).Rows(0)("EMPRESA_NUMERO").ToString
                    .txComplemento.Text = fs.Tables(TABELA).Rows(0)("EMPRESA_COMPLEMENTO").ToString
                    .txBairro.Text = fs.Tables(TABELA).Rows(0)("EMPRESA_BAIRRO").ToString
                    .txCEP.Text = fs.Tables(TABELA).Rows(0)("EMPRESA_CEP").ToString
                    .txCidade.Text = fs.Tables(TABELA).Rows(0)("EMPRESA_CIDADE").ToString
                    .txTelefone.Text = fs.Tables(TABELA).Rows(0)("EMPRESA_TELEFONE").ToString
                    .txUF.Text = fs.Tables(TABELA).Rows(0)("EMPRESA_UF").ToString
                    .txEmail.Text = fs.Tables(TABELA).Rows(0)("EMPRESA_EMAIL").ToString
                    .picImagem.Image = ConverteByteParaImagem(fs.Tables(TABELA).Rows(0)("EMPRESA_LOGOTIPO"))
                    .picImagem.SizeMode = PictureBoxSizeMode.StretchImage
                    .txtimg.Text = "IMAGEM_BD"
                    .MdiParent = frmMenuPrincipal
                    .Show()
                End With
            End If
        End If
    End Sub

    'Carregar A Grid
    Private Sub btnEditar_Click(sender As Object, e As EventArgs) Handles btnEditar.Click
        EditarEmpresas()
    End Sub

    'Salvar a Grid
    Private Sub btnSalvarGrid_Click(sender As Object, e As EventArgs) Handles btnSalvarGrid.Click
        salvarGrid(gcEmpresas, "GridEmpresas")
    End Sub
    Private Sub btnExcluirGrid_Click(sender As Object, e As EventArgs) Handles btnExcluirGrid.Click

    End Sub
    'Exportar Excel
    Private Sub btnExportarExcel_Click(sender As Object, e As EventArgs) Handles btnExportarExcel.Click
        exportarExcel(gcEmpresas, "Empresas")
    End Sub
    'Exportar HTML
    Private Sub btnExportarHTML_Click(sender As Object, e As EventArgs) Handles btnExportarHTML.Click
        exportarHTML(gcEmpresas, "Empresas")
    End Sub
    'Exportar PDF
    Private Sub btnExportarPDF_Click(sender As Object, e As EventArgs) Handles btnExportarPDF.Click
        exportarPDF(gcEmpresas, "Empresas")
    End Sub
    'Exportar Texto
    Private Sub btnExportarTexto_Click(sender As Object, e As EventArgs) Handles btnExportarTexto.Click
        exportarTexto(gcEmpresas, "Empresas")
    End Sub

    Private Sub btnExcluir_Click(sender As Object, e As EventArgs) Handles btnExcluir.Click
        If GridView1.RowCount > 0 Then
            Dim result = MessageBox.Show("Deseja excluir a empresa selecionada?", "Exclusão", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = MsgBoxResult.Yes Then
                Dim ID As String = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), gEmpresaCodigo)
                SQL = "DELETE FROM EMPRESAS WHERE EMPRESA_ID=" & ID
                TABELA = "TABELA"
                CONEXAO_FIREBIRD(SQL)
                MsgBox("Empresa excluída com sucesso!", vbInformation)
            Else
                Exit Sub
            End If
        Else
            MsgBox("Sem dados para excluir!")
        End If



    End Sub


End Class