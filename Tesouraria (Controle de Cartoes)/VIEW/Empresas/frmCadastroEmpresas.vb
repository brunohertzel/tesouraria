﻿Imports System.Net
Imports System.Web
Imports System.Runtime.CompilerServices
Imports System.Xml
Imports System.Text
Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Collections
Imports System.Drawing
Imports Newtonsoft.Json
Imports FirebirdSql.Data.FirebirdClient
Public Class frmCadastroEmpresas
    'Variáveis de campos

    Dim RAZAO_SOCIAL As String = ""
    Dim NOME_FANTASIA As String = ""
    Dim CNPJ As String = ""
    Dim INSCRICAO As String = ""
    Dim LOGRADOURO As String = ""
    Dim NUMERO As String = ""
    Dim COMPLEMENTO As String = ""
    Dim BAIRRO As String = ""
    Dim CIDADE As String = ""
    Dim CEP As String = ""
    Dim UF As String = ""
    Dim TELEFONE As String = ""
    Dim EMAIL As String = ""
    Dim STATUS As String = ""


    Private Sub btnBuscaReceita_Click(sender As Object, e As EventArgs) Handles btnBuscaReceita.Click

        Dim webclient As New WebClient
        webclient.Encoding = Encoding.UTF8
        Dim response = webclient.DownloadString(New Uri("https://www.receitaws.com.br/v1/cnpj/" & Replace(Replace(Replace(Replace(txCNPJ.Text, "/", ""), "-", ""), ".", ""), ",", "")))
        Dim doc As XmlDocument = JsonConvert.DeserializeXmlNode(response, "root")
        doc.Save("./CNPJ.xml")
        Dim Ds As New DataSet
        'Define o caminho do arquivo XML 
        Dim verifica As Boolean = True
        Ds.ReadXml("./CNPJ.xml")
        'Validação do Retorno da receita
        If Ds.Tables(0).Rows(0)("status").ToString() = "ERROR" Then
            MsgBox("Não foi possível obter os dados da Receita Federal!" & vbCrLf & "Motivo: " & Ds.Tables(0).Rows(0)("message").ToString(), vbCritical)
        Else
            If Ds.Tables(0).Rows(0)("situacao").ToString() <> "ATIVA" Then
                Dim ask As Double
                ask = MessageBox.Show("Empresa está com a situação " & Ds.Tables(0).Rows(0)("situacao").ToString() & ". Deseja importar os dados assim mesmo? ", "AM SOFT Gestor", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                If ask = MsgBoxResult.No Then
                    Exit Sub
                Else
                    verifica = False
                End If
            End If
            txRazaoSocial.Text = Ds.Tables(0).Rows(0)("nome").ToString
            txNomeFantasia.Text = Ds.Tables(0).Rows(0)("fantasia").ToString
            txLogradouro.Text = Ds.Tables(0).Rows(0)("logradouro").ToString
            txNumero.Text = Ds.Tables(0).Rows(0)("numero").ToString
            txBairro.Text = Ds.Tables(0).Rows(0)("bairro").ToString
            txComplemento.Text = Ds.Tables(0).Rows(0)("complemento").ToString
            txCEP.Text = Replace(Ds.Tables(0).Rows(0)("cep").ToString, ".", "")
            txCidade.Text = Ds.Tables(0).Rows(0)("municipio").ToString
            txUF.Text = Ds.Tables(0).Rows(0)("uf").ToString
            txTelefone.Text = Ds.Tables(0).Rows(0)("telefone").ToString
            txEmail.Text = Ds.Tables(0).Rows(0)("email").ToString
        End If

    End Sub

    Private Sub btnIncluir_Click(sender As Object, e As EventArgs) Handles btnIncluir.Click
        Limpar(Me)
        btnIncluir.Enabled = False
        btnEditar.Enabled = False
    End Sub

    'Fechar Aplicação
    Private Sub btnVoltar_Click(sender As Object, e As EventArgs) Handles btnVoltar.Click
        Me.Close()
    End Sub
    'Busca Endereço pelo CEP
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If txLogradouro.Text <> String.Empty Then
            Dim resultado = MessageBox.Show("Deseja sobrepor os dados do endereço atual?", "Soft Soluções", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If resultado = MsgBoxResult.No Then
                Exit Sub
            Else
                BuscaCep(txCEP.Text, txLogradouro, txCidade, txUF, txBairro)
            End If
        Else
            BuscaCep(txCEP.Text, txLogradouro, txCidade, txUF, txBairro)
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnBuscarImagem.Click
        Dim ofd1 As New OpenFileDialog
        ofd1.Multiselect = True
        ofd1.Title = "Selecionar Imagems"
        ofd1.InitialDirectory = "C:\Users\users\Pictures"
        ofd1.Filter = "Images (*.BMP;*.JPG;*.PNG)|*.BMP;*.JPG;*.PNG)"
        ofd1.CheckFileExists = True
        ofd1.CheckPathExists = True
        ofd1.FilterIndex = 2
        ofd1.RestoreDirectory = True
        ofd1.ReadOnlyChecked = True
        ofd1.ShowReadOnly = True
        Dim dr As DialogResult = ofd1.ShowDialog()

        If dr = System.Windows.Forms.DialogResult.OK Then
            picImagem.SizeMode = PictureBoxSizeMode.StretchImage
            picImagem.Image = Image.FromFile(ofd1.FileName)
            txtimg.ReadOnly = False
            txtimg.Text = ofd1.FileName
            txtimg.ReadOnly = True
            'btnConverteImagem.Enabled = True
        End If
    End Sub

    Private Sub btnApagarImagem_Click(sender As Object, e As EventArgs) Handles btnApagarImagem.Click
        picImagem.Image = Nothing
        txtimg.Text = String.Empty
    End Sub

    Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs) Handles btnSalvar.Click

        CARREGAR_INI()
        Dim arqImg As FileStream = Nothing
        Dim rImg As StreamReader = Nothing
        If Len(txtimg.Text) <> 0 Then
            If txtimg.Text <> "IMAGEM_BD" Then
                arqImg = New FileStream(txtimg.Text, FileMode.Open, FileAccess.Read, FileShare.Read)
                rImg = New StreamReader(arqImg)
            End If
        End If

        If ENTRADA_DADOS = "E" Then
            SQL = "UPDATE OR INSERT INTO EMPRESAS (EMPRESA_ID, "

        ElseIf ENTRADA_DADOS = "I" Then
            SQL = "INSERT INTO EMPRESAS ("
        End If

        SQL += "EMPRESA_NOME, EMPRESA_NOME_FANTASIA, " _
               & "EMPRESA_CNPJ, EMPRESA_INSCRICAO, EMPRESA_LOGRADOURO, " _
               & "EMPRESA_NUMERO, EMPRESA_COMPLEMENTO,EMPRESA_BAIRRO, EMPRESA_CEP, " _
               & "EMPRESA_CIDADE, EMPRESA_UF, EMPRESA_TELEFONE, EMPRESA_EMAIL, " _
               & "EMPRESA_STATUS"

        If txtimg.Text <> "IMAGEM_BD" Then
            SQL += ", EMPRESA_LOGOTIPO ) VALUES ("
        Else
            SQL += ") VALUES ("
        End If


        If ENTRADA_DADOS = "E" Then
            SQL += "@EMPRESA_ID, "
        End If

        SQL += "@EMPRESA_NOME, @EMPRESA_NOME_FANTASIA, @EMPRESA_CNPJ, @EMPRESA_INSCRICAO, @EMPRESA_LOGRADOURO, @EMPRESA_NUMERO, @EMPRESA_COMPLEMENTO, @EMPRESA_BAIRRO, " _
        & "@EMPRESA_CEP, @EMPRESA_CIDADE, @EMPRESA_UF, @EMPRESA_TELEFONE, @EMPRESA_EMAIL, @EMPRESA_STATUS"

        If txtimg.Text <> "IMAGEM_BD" Then
            SQL += ", @EMPRESA_LOGOTIPO )"
        Else
            SQL += ")"
        End If

        Dim strCon As String = "User=" & FIREBIRD_USUARIO & ";Password=" & FIREBIRD_SENHA & ";Database=" & FIREBIRD_BANCO & ";DataSource=" & FIREBIRD_SERVIDOR & ";Port=" & FIREBIRD_PORTA & ";Dialect=3;charset=WIN1252;"
        Dim Conexao As New FbConnection(strCon)
        Dim Comando As New FbCommand(SQL, Conexao)

        If ENTRADA_DADOS = "I" Then
            SQL += "RETURNING EMPRESA_ID;"
            Comando.Parameters.Add("EMPRESA_ID", FbDbType.Integer).Direction = ParameterDirection.Output

        ElseIf ENTRADA_DADOS = "E" Then
            SQL += " MATCHING EMPRESA_ID"
            Comando.Parameters.Add("@EMPRESA_ID", FbDbType.Integer).Value = txEmpresaCod.Text
        End If

        Comando.Parameters.Add("@EMPRESA_NOME", FbDbType.VarChar).Value = txRazaoSocial.Text
        Comando.Parameters.Add("@EMPRESA_NOME_FANTASIA", FbDbType.VarChar).Value = txNomeFantasia.Text
        Comando.Parameters.Add("@EMPRESA_CNPJ", FbDbType.VarChar).Value = txCNPJ.Text
        Comando.Parameters.Add("@EMPRESA_INSCRICAO", FbDbType.VarChar).Value = txInscricao.Text
        Comando.Parameters.Add("@EMPRESA_LOGRADOURO", FbDbType.VarChar).Value = txLogradouro.Text
        Comando.Parameters.Add("@EMPRESA_NUMERO", FbDbType.VarChar).Value = txNumero.Text
        Comando.Parameters.Add("@EMPRESA_COMPLEMENTO", FbDbType.VarChar).Value = txComplemento.Text
        Comando.Parameters.Add("@EMPRESA_BAIRRO", FbDbType.VarChar).Value = txBairro.Text
        Comando.Parameters.Add("@EMPRESA_CEP", FbDbType.VarChar).Value = txCEP.Text
        Comando.Parameters.Add("@EMPRESA_CIDADE", FbDbType.VarChar).Value = txCidade.Text
        Comando.Parameters.Add("@EMPRESA_UF", FbDbType.VarChar).Value = txUF.Text
        Comando.Parameters.Add("@EMPRESA_TELEFONE", FbDbType.VarChar).Value = txTelefone.Text
        Comando.Parameters.Add("@EMPRESA_EMAIL", FbDbType.VarChar).Value = txEmail.Text
        Comando.Parameters.Add("@EMPRESA_STATUS", FbDbType.Char).Value = "A"

        Try
            If Len(txtimg.Text) <> 0 Then
                If txtimg.Text = "IMAGEM_BD" Then
                    Replace(SQL, ", @EMPRESA_LOGOTIPO", " ")
                    Replace(SQL, ", EMPRESA_LOGOTIPO", " ")
                Else
                    Dim arqByteArray(arqImg.Length - 1) As Byte
                    arqImg.Read(arqByteArray, 0, arqImg.Length)
                    Comando.Parameters.Add("@EMPRESA_LOGOTIPO", FbDbType.Binary, arqImg.Length).Value = arqByteArray
                End If
            Else
                Comando.Parameters.Add("@EMPRESA_LOGOTIPO", FbDbType.VarChar).Value = ""
            End If
        Catch ex As Exception
        End Try

        Conexao.Open()
        Comando.ExecuteNonQuery()

        If ENTRADA_DADOS = "I" Then
            txEmpresaCod.Text = Convert.ToInt32(Comando.Parameters("EMPRESA_ID").Value)
        End If
        Conexao.Close()

        If ENTRADA_DADOS = "I" Then
            MsgBox("Empresa Incluída com Sucesso!")
            logDado = "Inclusão da Empresa Código: " & txEmpresaCod.Text & ", Razão Social: " & txRazaoSocial.Text & ", CNPJ: " & txCNPJ.Text
            ENTRADA_DADOS = "E"
            VALIDA_CAMPOS_CADASTRO()

        ElseIf ENTRADA_DADOS = "E" Then

            logDado = "Alteração da Empresa Código: " & txEmpresaCod.Text & "."
            logDado += " Dados Alterados:"

            ALTERACOES = "N"

            'Valida Alterações de campos
            VERIFICA_LOG(" CNPJ", CNPJ, txCNPJ.Text)
            VERIFICA_LOG(" Inscrição Estadual", INSCRICAO, txInscricao.Text)
            VERIFICA_LOG(" Razão Social", RAZAO_SOCIAL, txRazaoSocial.Text)
            VERIFICA_LOG(" Nome Fantasia", NOME_FANTASIA, txNomeFantasia.Text)
            VERIFICA_LOG(" Logradouro", LOGRADOURO, txLogradouro.Text)
            VERIFICA_LOG(" Número", NUMERO, txNumero.Text)
            VERIFICA_LOG(" Complemento", COMPLEMENTO, txComplemento.Text)
            VERIFICA_LOG(" Bairro", BAIRRO, txBairro.Text)
            VERIFICA_LOG(" Cidade", CIDADE, txCidade.Text)
            VERIFICA_LOG(" UF", UF, txUF.Text)
            VERIFICA_LOG(" CEP", CEP, txCEP.Text)
            VERIFICA_LOG(" Telefone", TELEFONE, txTelefone.Text)
            VERIFICA_LOG(" e-mail", EMAIL, txEmail.Text)
            If rbAtivo.Checked = True Then
                VERIFICA_LOG(" Status", STATUS, "A")
            ElseIf rbInativo.Checked = True Then
                VERIFICA_LOG(" Status", STATUS, "I")
            End If

            If ALTERACOES = "N" Then
                MsgBox("Não Houve alterações de dados!")
            ElseIf ALTERACOES = "S" Then
                MsgBox("Empresa alterada com Sucesso!")
                logTipo = ENTRADA_DADOS
                GravarLog()
            End If
        End If

        btnIncluir.Enabled = True
        btnEditar.Enabled = True

    End Sub

    Private Sub frmCadastroEmpresas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.FormBorderStyle = FormBorderStyle.None
        Me.WindowState = FormWindowState.Normal
        Me.WindowState = FormWindowState.Maximized
        CARREGAR_INI()
        abreform(ToolStrip1)
        btnIncluir.Enabled = False
        btnEditar.Enabled = False
        VALIDA_CAMPOS_CADASTRO()

    End Sub


    Sub VALIDA_CAMPOS_CADASTRO()
        If ENTRADA_DADOS = "I" Then
            RAZAO_SOCIAL = ""
            NOME_FANTASIA = ""
            CNPJ = ""
            INSCRICAO = ""
            LOGRADOURO = ""
            NUMERO = ""
            COMPLEMENTO = ""
            BAIRRO = ""
            CIDADE = ""
            CEP = ""
            UF = ""
            TELEFONE = ""
            EMAIL = ""
            STATUS = ""
        ElseIf ENTRADA_DADOS = "E" Then
            RAZAO_SOCIAL = txRazaoSocial.Text
            NOME_FANTASIA = txNomeFantasia.Text
            CNPJ = txCNPJ.Text
            INSCRICAO = txInscricao.Text
            LOGRADOURO = txLogradouro.Text
            NUMERO = txNumero.Text
            COMPLEMENTO = txComplemento.Text
            BAIRRO = txBairro.Text
            CIDADE = txCidade.Text
            CEP = txCEP.Text
            UF = txUF.Text
            TELEFONE = txTelefone.Text
            EMAIL = txEmail.Text
            STATUS = ""
            If rbAtivo.Checked = True Then
                STATUS = "A"
            ElseIf rbInativo.Checked = True Then
                STATUS = "I"
            End If
        End If
    End Sub


End Class