﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCadastroEmpresasLista
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gcEmpresas = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gEmpresaCodigo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gRazao = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gNomeFantasia = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gCNPJ = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gInscricao = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gLogrdouro = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gNumero = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gBairro = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gCidade = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gUF = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gTelefone = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gStatus = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rbInativo = New System.Windows.Forms.RadioButton()
        Me.rbAtivo = New System.Windows.Forms.RadioButton()
        Me.lbPesquisa = New System.Windows.Forms.Label()
        Me.btnLocalizar = New System.Windows.Forms.Button()
        Me.txBusca = New System.Windows.Forms.TextBox()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.btnVoltar = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnIncluir = New System.Windows.Forms.ToolStripButton()
        Me.btnEditar = New System.Windows.Forms.ToolStripButton()
        Me.btnExcluir = New System.Windows.Forms.ToolStripButton()
        Me.btnGrade = New System.Windows.Forms.ToolStripSplitButton()
        Me.btnSalvarGrid = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExcluirGrid = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnExportarExcel = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExportarHTML = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExportarPDF = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExportarTexto = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.gcEmpresas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'gcEmpresas
        '
        Me.gcEmpresas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcEmpresas.Location = New System.Drawing.Point(0, 127)
        Me.gcEmpresas.MainView = Me.GridView1
        Me.gcEmpresas.Name = "gcEmpresas"
        Me.gcEmpresas.Size = New System.Drawing.Size(1178, 531)
        Me.gcEmpresas.TabIndex = 5
        Me.gcEmpresas.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gEmpresaCodigo, Me.gRazao, Me.gNomeFantasia, Me.gCNPJ, Me.gInscricao, Me.gLogrdouro, Me.gNumero, Me.gBairro, Me.gCidade, Me.gUF, Me.gTelefone, Me.gStatus})
        Me.GridView1.GridControl = Me.gcEmpresas
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        Me.GridView1.OptionsView.ShowFooter = True
        '
        'gEmpresaCodigo
        '
        Me.gEmpresaCodigo.Caption = "Cód. Empresa"
        Me.gEmpresaCodigo.FieldName = "EMPRESA_ID"
        Me.gEmpresaCodigo.Name = "gEmpresaCodigo"
        Me.gEmpresaCodigo.OptionsColumn.AllowEdit = False
        Me.gEmpresaCodigo.OptionsColumn.ReadOnly = True
        Me.gEmpresaCodigo.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "EMPRESA_ID", "Empresas - {0}")})
        Me.gEmpresaCodigo.Visible = True
        Me.gEmpresaCodigo.VisibleIndex = 0
        '
        'gRazao
        '
        Me.gRazao.Caption = "Razão Social"
        Me.gRazao.FieldName = "EMPRESA_NOME"
        Me.gRazao.Name = "gRazao"
        Me.gRazao.OptionsColumn.AllowEdit = False
        Me.gRazao.OptionsColumn.ReadOnly = True
        Me.gRazao.Visible = True
        Me.gRazao.VisibleIndex = 1
        Me.gRazao.Width = 332
        '
        'gNomeFantasia
        '
        Me.gNomeFantasia.Caption = "Nome Fantasia"
        Me.gNomeFantasia.FieldName = "EMPRESA_NOME_FANTASIA"
        Me.gNomeFantasia.Name = "gNomeFantasia"
        Me.gNomeFantasia.OptionsColumn.AllowEdit = False
        Me.gNomeFantasia.OptionsColumn.ReadOnly = True
        Me.gNomeFantasia.Visible = True
        Me.gNomeFantasia.VisibleIndex = 2
        Me.gNomeFantasia.Width = 201
        '
        'gCNPJ
        '
        Me.gCNPJ.Caption = "CNPJ"
        Me.gCNPJ.FieldName = "EMPRESA_CNPJ"
        Me.gCNPJ.Name = "gCNPJ"
        Me.gCNPJ.OptionsColumn.AllowEdit = False
        Me.gCNPJ.OptionsColumn.ReadOnly = True
        Me.gCNPJ.Visible = True
        Me.gCNPJ.VisibleIndex = 3
        Me.gCNPJ.Width = 141
        '
        'gInscricao
        '
        Me.gInscricao.Caption = "Inscrição Estadual"
        Me.gInscricao.FieldName = "EMPRESA_INSCRICAO"
        Me.gInscricao.Name = "gInscricao"
        Me.gInscricao.OptionsColumn.AllowEdit = False
        Me.gInscricao.OptionsColumn.ReadOnly = True
        Me.gInscricao.Visible = True
        Me.gInscricao.VisibleIndex = 4
        Me.gInscricao.Width = 102
        '
        'gLogrdouro
        '
        Me.gLogrdouro.Caption = "Endereço"
        Me.gLogrdouro.FieldName = "EMPRESA_LOGRADOURO"
        Me.gLogrdouro.Name = "gLogrdouro"
        Me.gLogrdouro.Visible = True
        Me.gLogrdouro.VisibleIndex = 5
        '
        'gNumero
        '
        Me.gNumero.Caption = "Número"
        Me.gNumero.FieldName = "EMPRESA_NUMERO"
        Me.gNumero.Name = "gNumero"
        Me.gNumero.Visible = True
        Me.gNumero.VisibleIndex = 6
        '
        'gBairro
        '
        Me.gBairro.Caption = "Bairro"
        Me.gBairro.FieldName = "EMPRESA_BAIRRO"
        Me.gBairro.Name = "gBairro"
        Me.gBairro.Visible = True
        Me.gBairro.VisibleIndex = 7
        '
        'gCidade
        '
        Me.gCidade.Caption = "Cidade"
        Me.gCidade.FieldName = "EMPRESA_CIDADE"
        Me.gCidade.Name = "gCidade"
        Me.gCidade.Visible = True
        Me.gCidade.VisibleIndex = 8
        '
        'gUF
        '
        Me.gUF.Caption = "UF"
        Me.gUF.FieldName = "EMPRESA_UF"
        Me.gUF.Name = "gUF"
        Me.gUF.Visible = True
        Me.gUF.VisibleIndex = 9
        '
        'gTelefone
        '
        Me.gTelefone.Caption = "Telefone"
        Me.gTelefone.FieldName = "EMPRESA_TELEFONE"
        Me.gTelefone.Name = "gTelefone"
        Me.gTelefone.Visible = True
        Me.gTelefone.VisibleIndex = 10
        '
        'gStatus
        '
        Me.gStatus.Caption = "Status"
        Me.gStatus.FieldName = "EMPRESA_STATUS"
        Me.gStatus.Name = "gStatus"
        Me.gStatus.Visible = True
        Me.gStatus.VisibleIndex = 11
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.lbPesquisa)
        Me.GroupBox1.Controls.Add(Me.btnLocalizar)
        Me.GroupBox1.Controls.Add(Me.txBusca)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Location = New System.Drawing.Point(0, 56)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1178, 71)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Cadastro de Empresas"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rbInativo)
        Me.GroupBox2.Controls.Add(Me.rbAtivo)
        Me.GroupBox2.Location = New System.Drawing.Point(451, 7)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(136, 58)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Status"
        '
        'rbInativo
        '
        Me.rbInativo.AutoSize = True
        Me.rbInativo.Location = New System.Drawing.Point(66, 24)
        Me.rbInativo.Name = "rbInativo"
        Me.rbInativo.Size = New System.Drawing.Size(62, 17)
        Me.rbInativo.TabIndex = 1
        Me.rbInativo.Text = "Inativos"
        Me.rbInativo.UseVisualStyleBackColor = True
        '
        'rbAtivo
        '
        Me.rbAtivo.AutoSize = True
        Me.rbAtivo.Checked = True
        Me.rbAtivo.Location = New System.Drawing.Point(6, 24)
        Me.rbAtivo.Name = "rbAtivo"
        Me.rbAtivo.Size = New System.Drawing.Size(54, 17)
        Me.rbAtivo.TabIndex = 0
        Me.rbAtivo.TabStop = True
        Me.rbAtivo.Text = "Ativos"
        Me.rbAtivo.UseVisualStyleBackColor = True
        '
        'lbPesquisa
        '
        Me.lbPesquisa.AutoSize = True
        Me.lbPesquisa.ForeColor = System.Drawing.Color.DimGray
        Me.lbPesquisa.Location = New System.Drawing.Point(6, 16)
        Me.lbPesquisa.Name = "lbPesquisa"
        Me.lbPesquisa.Size = New System.Drawing.Size(238, 13)
        Me.lbPesquisa.TabIndex = 2
        Me.lbPesquisa.Text = "Digite o Código ou a descrição a ser pesquisado."
        '
        'btnLocalizar
        '
        Me.btnLocalizar.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Search_icon
        Me.btnLocalizar.Location = New System.Drawing.Point(372, 31)
        Me.btnLocalizar.Name = "btnLocalizar"
        Me.btnLocalizar.Size = New System.Drawing.Size(44, 31)
        Me.btnLocalizar.TabIndex = 1
        Me.btnLocalizar.UseVisualStyleBackColor = True
        '
        'txBusca
        '
        Me.txBusca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txBusca.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txBusca.Location = New System.Drawing.Point(6, 31)
        Me.txBusca.Name = "txBusca"
        Me.txBusca.Size = New System.Drawing.Size(360, 31)
        Me.txBusca.TabIndex = 0
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.Color.SteelBlue
        Me.ToolStrip1.GripMargin = New System.Windows.Forms.Padding(3, 3, 4, 3)
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(34, 34)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnVoltar, Me.ToolStripSeparator1, Me.btnIncluir, Me.btnEditar, Me.btnExcluir, Me.btnGrade})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Margin = New System.Windows.Forms.Padding(4)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1178, 56)
        Me.ToolStrip1.TabIndex = 3
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'btnVoltar
        '
        Me.btnVoltar.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Log_Out_Icon_32
        Me.btnVoltar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnVoltar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnVoltar.Name = "btnVoltar"
        Me.btnVoltar.Size = New System.Drawing.Size(41, 53)
        Me.btnVoltar.Text = "&Voltar"
        Me.btnVoltar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnVoltar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 56)
        '
        'btnIncluir
        '
        Me.btnIncluir.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources._1489198437_Add
        Me.btnIncluir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnIncluir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnIncluir.Name = "btnIncluir"
        Me.btnIncluir.Size = New System.Drawing.Size(44, 53)
        Me.btnIncluir.Text = "&Incluir"
        Me.btnIncluir.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnIncluir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnEditar
        '
        Me.btnEditar.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Edit_page
        Me.btnEditar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(41, 53)
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnExcluir
        '
        Me.btnExcluir.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.ic_delete_128_28267
        Me.btnExcluir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnExcluir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnExcluir.Name = "btnExcluir"
        Me.btnExcluir.Size = New System.Drawing.Size(45, 53)
        Me.btnExcluir.Text = "Excluir"
        Me.btnExcluir.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnExcluir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnGrade
        '
        Me.btnGrade.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnSalvarGrid, Me.btnExcluirGrid, Me.ToolStripSeparator2, Me.btnExportarExcel, Me.btnExportarHTML, Me.btnExportarPDF, Me.btnExportarTexto})
        Me.btnGrade.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.salva_grade48
        Me.btnGrade.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnGrade.Name = "btnGrade"
        Me.btnGrade.Size = New System.Drawing.Size(54, 53)
        Me.btnGrade.Text = "Grade"
        Me.btnGrade.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnSalvarGrid
        '
        Me.btnSalvarGrid.Name = "btnSalvarGrid"
        Me.btnSalvarGrid.Size = New System.Drawing.Size(153, 22)
        Me.btnSalvarGrid.Text = "Salvar Grade"
        '
        'btnExcluirGrid
        '
        Me.btnExcluirGrid.Name = "btnExcluirGrid"
        Me.btnExcluirGrid.Size = New System.Drawing.Size(153, 22)
        Me.btnExcluirGrid.Text = "Excluir Grade"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(150, 6)
        '
        'btnExportarExcel
        '
        Me.btnExportarExcel.Name = "btnExportarExcel"
        Me.btnExportarExcel.Size = New System.Drawing.Size(153, 22)
        Me.btnExportarExcel.Text = "Exportar Excel"
        '
        'btnExportarHTML
        '
        Me.btnExportarHTML.Name = "btnExportarHTML"
        Me.btnExportarHTML.Size = New System.Drawing.Size(153, 22)
        Me.btnExportarHTML.Text = "Exportar HTML"
        '
        'btnExportarPDF
        '
        Me.btnExportarPDF.Name = "btnExportarPDF"
        Me.btnExportarPDF.Size = New System.Drawing.Size(153, 22)
        Me.btnExportarPDF.Text = "Exportar PDF"
        '
        'btnExportarTexto
        '
        Me.btnExportarTexto.Name = "btnExportarTexto"
        Me.btnExportarTexto.Size = New System.Drawing.Size(153, 22)
        Me.btnExportarTexto.Text = "Exportar Texto"
        '
        'frmCadastroEmpresasLista
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1178, 658)
        Me.Controls.Add(Me.gcEmpresas)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Name = "frmCadastroEmpresasLista"
        Me.Text = "Cadastro Empresas Lista"
        CType(Me.gcEmpresas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents gcEmpresas As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents rbInativo As RadioButton
    Friend WithEvents rbAtivo As RadioButton
    Friend WithEvents lbPesquisa As Label
    Friend WithEvents btnLocalizar As Button
    Friend WithEvents txBusca As TextBox
    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents btnVoltar As ToolStripButton
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents btnIncluir As ToolStripButton
    Friend WithEvents btnEditar As ToolStripButton
    Friend WithEvents btnGrade As ToolStripSplitButton
    Friend WithEvents btnSalvarGrid As ToolStripMenuItem
    Friend WithEvents btnExcluirGrid As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents btnExportarExcel As ToolStripMenuItem
    Friend WithEvents btnExportarHTML As ToolStripMenuItem
    Friend WithEvents btnExportarPDF As ToolStripMenuItem
    Friend WithEvents btnExportarTexto As ToolStripMenuItem
    Friend WithEvents gEmpresaCodigo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gRazao As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gNomeFantasia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gCNPJ As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gInscricao As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gLogrdouro As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gNumero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gBairro As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gCidade As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gUF As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gTelefone As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gStatus As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnExcluir As ToolStripButton
End Class
