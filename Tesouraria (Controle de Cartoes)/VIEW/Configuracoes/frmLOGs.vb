﻿Public Class frmLOGs
    'Eventos de Abertura do FORM
    Private Sub frmLOGs_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.FormBorderStyle = FormBorderStyle.None
        Me.WindowState = FormWindowState.Normal
        Me.WindowState = FormWindowState.Maximized
        CARREGAR_INI()
        abreform(ToolStrip1)
    End Sub

    'Evento do Botão Voltar
    Private Sub btnVoltar_Click(sender As Object, e As EventArgs) Handles btnVoltar.Click
        Me.Close()
    End Sub

    Private Sub ToolStripButton2_Click(sender As Object, e As EventArgs) Handles ToolStripButton2.Click
        SQL = "SELECT
                L.log_id,
                l.log_data,
                l.log_hora,
                l.log_descricao,
                l.log_usuario,
                l.log_ip_origem,
                l.log_estacao,
                CASE
                    WHEN L.log_tipo='LI' THEN 'Login'
                    WHEN L.log_tipo='LO' THEN 'Logout'
                    WHEN L.log_tipo='I' THEN 'Inclusão'
                    WHEN L.log_tipo='E' THEN 'Edição'
                    WHEN L.log_tipo='EX' THEN 'Exclusão'
                    WHEN L.log_tipo='IM' THEN 'Impressão'
                    WHEN L.log_tipo='IE' THEN 'Importação'
                    WHEN L.log_tipo='GR' THEN 'Gravação'
                END AS LOG_TIPO,
                case
                    WHEN L.log_usuario='0' THEN 'ROOT (MESTRE)'
                    WHEN L.LOG_USUARIO<>'0' THEN U.usuario_nome
                END AS USUARIO_NOME 
            FROM LOGS L 
            LEFT OUTER JOIN USUARIOS U ON L.LOG_USUARIO=U.USUARIO_ID 
            WHERE UPPER(LOG_DESCRICAO) LIKE '%" & txBusca.Text.ToUpper & "%' "
        If dtInicial.Text <> "" And dtFinal.Text <> "" Then
            SQL += " AND LOG_DATA BETWEEN '" & Format(dtInicial.EditValue, "yyyy.MM.dd") & "' AND '" & Format(dtInicial.EditValue, "yyyy.MM.dd") & "'"
        End If
        TABELA = "TABELA"
        CONEXAO_FIREBIRD(SQL)
        gcLOGs.DataSource = fs.Tables(TABELA)
    End Sub

    'Carregar A Grid


    'Salvar a Grid
    Private Sub btnSalvarGrid_Click(sender As Object, e As EventArgs) Handles btnSalvarGrid.Click
        salvarGrid(gcLOGs, "GridLogs")
    End Sub
    Private Sub btnExcluirGrid_Click(sender As Object, e As EventArgs) Handles btnExcluirGrid.Click

    End Sub
    'Exportar Excel
    Private Sub btnExportarExcel_Click(sender As Object, e As EventArgs) Handles btnExportarExcel.Click
        exportarExcel(gcLOGs, "LOGs")
    End Sub
    'Exportar HTML
    Private Sub btnExportarHTML_Click(sender As Object, e As EventArgs) Handles btnExportarHTML.Click
        exportarHTML(gcLOGs, "LOGs")
    End Sub
    'Exportar PDF
    Private Sub btnExportarPDF_Click(sender As Object, e As EventArgs) Handles btnExportarPDF.Click
        exportarPDF(gcLOGs, "LOGs")
    End Sub
    'Exportar Texto
    Private Sub btnExportarTexto_Click(sender As Object, e As EventArgs) Handles btnExportarTexto.Click
        exportarTexto(gcLOGs, "LOGs")
    End Sub




End Class