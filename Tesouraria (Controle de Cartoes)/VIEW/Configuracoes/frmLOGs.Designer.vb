﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmLOGs
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.btnVoltar = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtFinal = New DevExpress.XtraEditors.DateEdit()
        Me.dtInicial = New DevExpress.XtraEditors.DateEdit()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.rbInativo = New System.Windows.Forms.RadioButton()
        Me.rbAtivo = New System.Windows.Forms.RadioButton()
        Me.lbPesquisa = New System.Windows.Forms.Label()
        Me.txBusca = New System.Windows.Forms.TextBox()
        Me.gcLOGs = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gSequencia = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gData = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gHora = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gLOG = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemMemoEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit()
        Me.gUsuarioCod = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gUsuarioNome = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gTipoLog = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gIpOrigem = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gNomeEstacao = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemMemoEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit()
        Me.btnGrade = New System.Windows.Forms.ToolStripSplitButton()
        Me.btnSalvarGrid = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExcluirGrid = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnExportarExcel = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExportarHTML = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExportarPDF = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExportarTexto = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStrip1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dtFinal.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtFinal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtInicial.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtInicial.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.gcLOGs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemMemoEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemMemoEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.SystemColors.ControlDark
        Me.ToolStrip1.GripMargin = New System.Windows.Forms.Padding(3, 3, 4, 3)
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(34, 34)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnVoltar, Me.ToolStripSeparator1, Me.ToolStripButton2, Me.btnGrade})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Margin = New System.Windows.Forms.Padding(4)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(981, 56)
        Me.ToolStrip1.TabIndex = 5
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'btnVoltar
        '
        Me.btnVoltar.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Log_Out_Icon_32
        Me.btnVoltar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnVoltar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnVoltar.Name = "btnVoltar"
        Me.btnVoltar.Size = New System.Drawing.Size(41, 53)
        Me.btnVoltar.Text = "&Voltar"
        Me.btnVoltar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnVoltar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 56)
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Search
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(57, 53)
        Me.ToolStripButton2.Text = "&Localizar"
        Me.ToolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.dtFinal)
        Me.GroupBox2.Controls.Add(Me.dtInicial)
        Me.GroupBox2.Controls.Add(Me.GroupBox3)
        Me.GroupBox2.Controls.Add(Me.lbPesquisa)
        Me.GroupBox2.Controls.Add(Me.txBusca)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox2.Location = New System.Drawing.Point(0, 56)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(981, 75)
        Me.GroupBox2.TabIndex = 7
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Cadastro de Cartões"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.DimGray
        Me.Label2.Location = New System.Drawing.Point(508, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Data Final"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.DimGray
        Me.Label1.Location = New System.Drawing.Point(372, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Data Inicial"
        '
        'dtFinal
        '
        Me.dtFinal.EditValue = Nothing
        Me.dtFinal.Location = New System.Drawing.Point(511, 30)
        Me.dtFinal.Name = "dtFinal"
        Me.dtFinal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtFinal.Properties.Appearance.Options.UseFont = True
        Me.dtFinal.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.dtFinal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtFinal.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtFinal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.dtFinal.Size = New System.Drawing.Size(141, 32)
        Me.dtFinal.TabIndex = 5
        '
        'dtInicial
        '
        Me.dtInicial.EditValue = Nothing
        Me.dtInicial.Location = New System.Drawing.Point(372, 30)
        Me.dtInicial.Name = "dtInicial"
        Me.dtInicial.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtInicial.Properties.Appearance.Options.UseFont = True
        Me.dtInicial.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.dtInicial.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtInicial.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtInicial.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.dtInicial.Size = New System.Drawing.Size(133, 32)
        Me.dtInicial.TabIndex = 4
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.rbInativo)
        Me.GroupBox3.Controls.Add(Me.rbAtivo)
        Me.GroupBox3.Location = New System.Drawing.Point(677, 11)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(136, 58)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Status"
        '
        'rbInativo
        '
        Me.rbInativo.AutoSize = True
        Me.rbInativo.Location = New System.Drawing.Point(66, 24)
        Me.rbInativo.Name = "rbInativo"
        Me.rbInativo.Size = New System.Drawing.Size(62, 17)
        Me.rbInativo.TabIndex = 1
        Me.rbInativo.Text = "Inativos"
        Me.rbInativo.UseVisualStyleBackColor = True
        '
        'rbAtivo
        '
        Me.rbAtivo.AutoSize = True
        Me.rbAtivo.Checked = True
        Me.rbAtivo.Location = New System.Drawing.Point(6, 24)
        Me.rbAtivo.Name = "rbAtivo"
        Me.rbAtivo.Size = New System.Drawing.Size(54, 17)
        Me.rbAtivo.TabIndex = 0
        Me.rbAtivo.TabStop = True
        Me.rbAtivo.Text = "Ativos"
        Me.rbAtivo.UseVisualStyleBackColor = True
        '
        'lbPesquisa
        '
        Me.lbPesquisa.AutoSize = True
        Me.lbPesquisa.ForeColor = System.Drawing.Color.DimGray
        Me.lbPesquisa.Location = New System.Drawing.Point(6, 16)
        Me.lbPesquisa.Name = "lbPesquisa"
        Me.lbPesquisa.Size = New System.Drawing.Size(238, 13)
        Me.lbPesquisa.TabIndex = 2
        Me.lbPesquisa.Text = "Digite o Código ou a descrição a ser pesquisado."
        '
        'txBusca
        '
        Me.txBusca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txBusca.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txBusca.Location = New System.Drawing.Point(6, 31)
        Me.txBusca.Name = "txBusca"
        Me.txBusca.Size = New System.Drawing.Size(360, 31)
        Me.txBusca.TabIndex = 0
        '
        'gcLOGs
        '
        Me.gcLOGs.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcLOGs.Location = New System.Drawing.Point(0, 131)
        Me.gcLOGs.MainView = Me.GridView1
        Me.gcLOGs.Name = "gcLOGs"
        Me.gcLOGs.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemMemoEdit1, Me.RepositoryItemMemoEdit2})
        Me.gcLOGs.Size = New System.Drawing.Size(981, 502)
        Me.gcLOGs.TabIndex = 8
        Me.gcLOGs.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.EvenRow.Options.UseTextOptions = True
        Me.GridView1.Appearance.EvenRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.GridView1.Appearance.OddRow.Options.UseTextOptions = True
        Me.GridView1.Appearance.OddRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.GridView1.Appearance.Row.Options.UseTextOptions = True
        Me.GridView1.Appearance.Row.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.GridView1.Appearance.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.GridView1.AppearancePrint.EvenRow.Options.UseTextOptions = True
        Me.GridView1.AppearancePrint.EvenRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.GridView1.AppearancePrint.Row.Options.UseTextOptions = True
        Me.GridView1.AppearancePrint.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.GridView1.ColumnPanelRowHeight = 3
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gSequencia, Me.gData, Me.gHora, Me.gLOG, Me.gUsuarioCod, Me.gUsuarioNome, Me.gTipoLog, Me.gIpOrigem, Me.gNomeEstacao})
        Me.GridView1.GridControl = Me.gcLOGs
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.AllowHtmlDrawHeaders = True
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
        Me.GridView1.OptionsView.RowAutoHeight = True
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.RowHeight = 3
        '
        'gSequencia
        '
        Me.gSequencia.Caption = "Sequência"
        Me.gSequencia.FieldName = "LOG_ID"
        Me.gSequencia.Name = "gSequencia"
        Me.gSequencia.OptionsColumn.AllowEdit = False
        Me.gSequencia.OptionsColumn.ReadOnly = True
        Me.gSequencia.Visible = True
        Me.gSequencia.VisibleIndex = 0
        '
        'gData
        '
        Me.gData.Caption = "Data"
        Me.gData.DisplayFormat.FormatString = "d"
        Me.gData.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.gData.FieldName = "LOG_DATA"
        Me.gData.Name = "gData"
        Me.gData.OptionsColumn.AllowEdit = False
        Me.gData.OptionsColumn.ReadOnly = True
        Me.gData.Visible = True
        Me.gData.VisibleIndex = 1
        '
        'gHora
        '
        Me.gHora.Caption = "Hora"
        Me.gHora.DisplayFormat.FormatString = "t"
        Me.gHora.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.gHora.FieldName = "LOG_HORA"
        Me.gHora.Name = "gHora"
        Me.gHora.OptionsColumn.AllowEdit = False
        Me.gHora.OptionsColumn.ReadOnly = True
        Me.gHora.Visible = True
        Me.gHora.VisibleIndex = 2
        '
        'gLOG
        '
        Me.gLOG.AppearanceCell.Options.UseTextOptions = True
        Me.gLOG.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.gLOG.Caption = "Descrição LOG"
        Me.gLOG.ColumnEdit = Me.RepositoryItemMemoEdit2
        Me.gLOG.FieldName = "LOG_DESCRICAO"
        Me.gLOG.MinWidth = 250
        Me.gLOG.Name = "gLOG"
        Me.gLOG.OptionsColumn.AllowEdit = False
        Me.gLOG.OptionsColumn.ReadOnly = True
        Me.gLOG.OptionsEditForm.RowSpan = 3
        Me.gLOG.Visible = True
        Me.gLOG.VisibleIndex = 3
        Me.gLOG.Width = 416
        '
        'RepositoryItemMemoEdit2
        '
        Me.RepositoryItemMemoEdit2.Name = "RepositoryItemMemoEdit2"
        '
        'gUsuarioCod
        '
        Me.gUsuarioCod.Caption = "Código Usuário"
        Me.gUsuarioCod.FieldName = "LOG_USUARIO"
        Me.gUsuarioCod.Name = "gUsuarioCod"
        Me.gUsuarioCod.OptionsColumn.AllowEdit = False
        Me.gUsuarioCod.OptionsColumn.ReadOnly = True
        Me.gUsuarioCod.Visible = True
        Me.gUsuarioCod.VisibleIndex = 4
        Me.gUsuarioCod.Width = 80
        '
        'gUsuarioNome
        '
        Me.gUsuarioNome.Caption = "Usuário"
        Me.gUsuarioNome.FieldName = "USUARIO_NOME"
        Me.gUsuarioNome.Name = "gUsuarioNome"
        Me.gUsuarioNome.OptionsColumn.AllowEdit = False
        Me.gUsuarioNome.OptionsColumn.ReadOnly = True
        Me.gUsuarioNome.Visible = True
        Me.gUsuarioNome.VisibleIndex = 5
        Me.gUsuarioNome.Width = 200
        '
        'gTipoLog
        '
        Me.gTipoLog.Caption = "Tipo de Operação"
        Me.gTipoLog.FieldName = "LOG_TIPO"
        Me.gTipoLog.Name = "gTipoLog"
        Me.gTipoLog.OptionsColumn.AllowEdit = False
        Me.gTipoLog.OptionsColumn.ReadOnly = True
        Me.gTipoLog.Visible = True
        Me.gTipoLog.VisibleIndex = 6
        Me.gTipoLog.Width = 200
        '
        'gIpOrigem
        '
        Me.gIpOrigem.Caption = "IP Origem"
        Me.gIpOrigem.FieldName = "LOG_IP_ORIGEM"
        Me.gIpOrigem.Name = "gIpOrigem"
        Me.gIpOrigem.Visible = True
        Me.gIpOrigem.VisibleIndex = 7
        Me.gIpOrigem.Width = 100
        '
        'gNomeEstacao
        '
        Me.gNomeEstacao.Caption = "Estação Origem"
        Me.gNomeEstacao.FieldName = "LOG_ESTACAO"
        Me.gNomeEstacao.Name = "gNomeEstacao"
        Me.gNomeEstacao.Visible = True
        Me.gNomeEstacao.VisibleIndex = 8
        Me.gNomeEstacao.Width = 150
        '
        'RepositoryItemMemoEdit1
        '
        Me.RepositoryItemMemoEdit1.Appearance.Options.UseTextOptions = True
        Me.RepositoryItemMemoEdit1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.RepositoryItemMemoEdit1.Name = "RepositoryItemMemoEdit1"
        '
        'btnGrade
        '
        Me.btnGrade.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnSalvarGrid, Me.btnExcluirGrid, Me.ToolStripSeparator2, Me.btnExportarExcel, Me.btnExportarHTML, Me.btnExportarPDF, Me.btnExportarTexto})
        Me.btnGrade.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.salva_grade48
        Me.btnGrade.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnGrade.Name = "btnGrade"
        Me.btnGrade.Size = New System.Drawing.Size(54, 53)
        Me.btnGrade.Text = "Grade"
        Me.btnGrade.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnSalvarGrid
        '
        Me.btnSalvarGrid.Name = "btnSalvarGrid"
        Me.btnSalvarGrid.Size = New System.Drawing.Size(153, 22)
        Me.btnSalvarGrid.Text = "Salvar Grade"
        '
        'btnExcluirGrid
        '
        Me.btnExcluirGrid.Name = "btnExcluirGrid"
        Me.btnExcluirGrid.Size = New System.Drawing.Size(153, 22)
        Me.btnExcluirGrid.Text = "Excluir Grade"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(150, 6)
        '
        'btnExportarExcel
        '
        Me.btnExportarExcel.Name = "btnExportarExcel"
        Me.btnExportarExcel.Size = New System.Drawing.Size(153, 22)
        Me.btnExportarExcel.Text = "Exportar Excel"
        '
        'btnExportarHTML
        '
        Me.btnExportarHTML.Name = "btnExportarHTML"
        Me.btnExportarHTML.Size = New System.Drawing.Size(153, 22)
        Me.btnExportarHTML.Text = "Exportar HTML"
        '
        'btnExportarPDF
        '
        Me.btnExportarPDF.Name = "btnExportarPDF"
        Me.btnExportarPDF.Size = New System.Drawing.Size(153, 22)
        Me.btnExportarPDF.Text = "Exportar PDF"
        '
        'btnExportarTexto
        '
        Me.btnExportarTexto.Name = "btnExportarTexto"
        Me.btnExportarTexto.Size = New System.Drawing.Size(153, 22)
        Me.btnExportarTexto.Text = "Exportar Texto"
        '
        'frmLOGs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(981, 633)
        Me.Controls.Add(Me.gcLOGs)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Name = "frmLOGs"
        Me.Text = "frmLOGs"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dtFinal.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtFinal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtInicial.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtInicial.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.gcLOGs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemMemoEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemMemoEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents btnVoltar As ToolStripButton
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents ToolStripButton2 As ToolStripButton
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents dtFinal As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dtInicial As DevExpress.XtraEditors.DateEdit
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents rbInativo As RadioButton
    Friend WithEvents rbAtivo As RadioButton
    Friend WithEvents lbPesquisa As Label
    Friend WithEvents txBusca As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents gcLOGs As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gData As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gHora As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gLOG As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gSequencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gUsuarioCod As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gUsuarioNome As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gTipoLog As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemMemoEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit
    Friend WithEvents RepositoryItemMemoEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit
    Friend WithEvents gIpOrigem As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gNomeEstacao As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnGrade As ToolStripSplitButton
    Friend WithEvents btnSalvarGrid As ToolStripMenuItem
    Friend WithEvents btnExcluirGrid As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents btnExportarExcel As ToolStripMenuItem
    Friend WithEvents btnExportarHTML As ToolStripMenuItem
    Friend WithEvents btnExportarPDF As ToolStripMenuItem
    Friend WithEvents btnExportarTexto As ToolStripMenuItem
End Class
