﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmConexoes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txFirebirdPorta = New System.Windows.Forms.TextBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txFirebirdSenha = New System.Windows.Forms.TextBox()
        Me.txFirebirdUsuario = New System.Windows.Forms.TextBox()
        Me.cbFirebirdBanco = New System.Windows.Forms.ComboBox()
        Me.cbFirebirdServidor = New System.Windows.Forms.ComboBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txFirebirdPorta2 = New System.Windows.Forms.TextBox()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txFirebirdSenha2 = New System.Windows.Forms.TextBox()
        Me.txFirebirdUsuario2 = New System.Windows.Forms.TextBox()
        Me.cbFirebirdBanco2 = New System.Windows.Forms.ComboBox()
        Me.cbFirebirdServidor2 = New System.Windows.Forms.ComboBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboColor02 = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboColor01 = New System.Windows.Forms.ComboBox()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog()
        Me.picColor02 = New System.Windows.Forms.PictureBox()
        Me.picColor01 = New System.Windows.Forms.PictureBox()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        CType(Me.picColor02, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picColor01, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.txFirebirdPorta)
        Me.GroupBox2.Controls.Add(Me.Button4)
        Me.GroupBox2.Controls.Add(Me.Button5)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.txFirebirdSenha)
        Me.GroupBox2.Controls.Add(Me.txFirebirdUsuario)
        Me.GroupBox2.Controls.Add(Me.cbFirebirdBanco)
        Me.GroupBox2.Controls.Add(Me.cbFirebirdServidor)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox2.Location = New System.Drawing.Point(0, 58)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1469, 113)
        Me.GroupBox2.TabIndex = 12
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "FIREBIRD SERVER"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(376, 76)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(35, 13)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Porta:"
        '
        'txFirebirdPorta
        '
        Me.txFirebirdPorta.Location = New System.Drawing.Point(428, 69)
        Me.txFirebirdPorta.Name = "txFirebirdPorta"
        Me.txFirebirdPorta.Size = New System.Drawing.Size(98, 20)
        Me.txFirebirdPorta.TabIndex = 7
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(296, 68)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(67, 21)
        Me.Button4.TabIndex = 6
        Me.Button4.Text = "Pesquisar"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(532, 22)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(72, 67)
        Me.Button5.TabIndex = 5
        Me.Button5.Text = "Gravar"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(376, 52)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Senha:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(376, 29)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(46, 13)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Usuário:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(15, 56)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(41, 13)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Banco:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(16, 16)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(49, 13)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "Servidor:"
        '
        'txFirebirdSenha
        '
        Me.txFirebirdSenha.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txFirebirdSenha.Location = New System.Drawing.Point(428, 45)
        Me.txFirebirdSenha.Name = "txFirebirdSenha"
        Me.txFirebirdSenha.Size = New System.Drawing.Size(98, 20)
        Me.txFirebirdSenha.TabIndex = 3
        '
        'txFirebirdUsuario
        '
        Me.txFirebirdUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txFirebirdUsuario.Location = New System.Drawing.Point(428, 22)
        Me.txFirebirdUsuario.Name = "txFirebirdUsuario"
        Me.txFirebirdUsuario.Size = New System.Drawing.Size(98, 20)
        Me.txFirebirdUsuario.TabIndex = 3
        '
        'cbFirebirdBanco
        '
        Me.cbFirebirdBanco.FormattingEnabled = True
        Me.cbFirebirdBanco.Location = New System.Drawing.Point(19, 69)
        Me.cbFirebirdBanco.Name = "cbFirebirdBanco"
        Me.cbFirebirdBanco.Size = New System.Drawing.Size(271, 21)
        Me.cbFirebirdBanco.TabIndex = 2
        '
        'cbFirebirdServidor
        '
        Me.cbFirebirdServidor.FormattingEnabled = True
        Me.cbFirebirdServidor.Location = New System.Drawing.Point(19, 32)
        Me.cbFirebirdServidor.Name = "cbFirebirdServidor"
        Me.cbFirebirdServidor.Size = New System.Drawing.Size(344, 21)
        Me.cbFirebirdServidor.TabIndex = 0
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.Label21)
        Me.GroupBox6.Controls.Add(Me.txFirebirdPorta2)
        Me.GroupBox6.Controls.Add(Me.Button12)
        Me.GroupBox6.Controls.Add(Me.Button14)
        Me.GroupBox6.Controls.Add(Me.Label22)
        Me.GroupBox6.Controls.Add(Me.Label23)
        Me.GroupBox6.Controls.Add(Me.Label24)
        Me.GroupBox6.Controls.Add(Me.Label25)
        Me.GroupBox6.Controls.Add(Me.txFirebirdSenha2)
        Me.GroupBox6.Controls.Add(Me.txFirebirdUsuario2)
        Me.GroupBox6.Controls.Add(Me.cbFirebirdBanco2)
        Me.GroupBox6.Controls.Add(Me.cbFirebirdServidor2)
        Me.GroupBox6.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox6.Location = New System.Drawing.Point(0, 171)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(1469, 113)
        Me.GroupBox6.TabIndex = 13
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "FIREBIRD SERVER BANCO 2"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(386, 79)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(35, 13)
        Me.Label21.TabIndex = 8
        Me.Label21.Text = "Porta:"
        '
        'txFirebirdPorta2
        '
        Me.txFirebirdPorta2.Location = New System.Drawing.Point(428, 71)
        Me.txFirebirdPorta2.Name = "txFirebirdPorta2"
        Me.txFirebirdPorta2.Size = New System.Drawing.Size(98, 20)
        Me.txFirebirdPorta2.TabIndex = 7
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(291, 69)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(72, 22)
        Me.Button12.TabIndex = 6
        Me.Button12.Text = "Pesquisar"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'Button14
        '
        Me.Button14.Location = New System.Drawing.Point(532, 23)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(72, 68)
        Me.Button14.TabIndex = 5
        Me.Button14.Text = "Gravar"
        Me.Button14.UseVisualStyleBackColor = True
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(381, 54)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(41, 13)
        Me.Label22.TabIndex = 4
        Me.Label22.Text = "Senha:"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(381, 31)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(46, 13)
        Me.Label23.TabIndex = 4
        Me.Label23.Text = "Usuário:"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(15, 56)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(41, 13)
        Me.Label24.TabIndex = 4
        Me.Label24.Text = "Banco:"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(16, 16)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(49, 13)
        Me.Label25.TabIndex = 4
        Me.Label25.Text = "Servidor:"
        '
        'txFirebirdSenha2
        '
        Me.txFirebirdSenha2.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txFirebirdSenha2.Location = New System.Drawing.Point(428, 46)
        Me.txFirebirdSenha2.Name = "txFirebirdSenha2"
        Me.txFirebirdSenha2.Size = New System.Drawing.Size(98, 20)
        Me.txFirebirdSenha2.TabIndex = 3
        '
        'txFirebirdUsuario2
        '
        Me.txFirebirdUsuario2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txFirebirdUsuario2.Location = New System.Drawing.Point(428, 23)
        Me.txFirebirdUsuario2.Name = "txFirebirdUsuario2"
        Me.txFirebirdUsuario2.Size = New System.Drawing.Size(98, 20)
        Me.txFirebirdUsuario2.TabIndex = 3
        '
        'cbFirebirdBanco2
        '
        Me.cbFirebirdBanco2.FormattingEnabled = True
        Me.cbFirebirdBanco2.Location = New System.Drawing.Point(19, 71)
        Me.cbFirebirdBanco2.Name = "cbFirebirdBanco2"
        Me.cbFirebirdBanco2.Size = New System.Drawing.Size(266, 21)
        Me.cbFirebirdBanco2.TabIndex = 2
        '
        'cbFirebirdServidor2
        '
        Me.cbFirebirdServidor2.FormattingEnabled = True
        Me.cbFirebirdServidor2.Location = New System.Drawing.Point(19, 32)
        Me.cbFirebirdServidor2.Name = "cbFirebirdServidor2"
        Me.cbFirebirdServidor2.Size = New System.Drawing.Size(344, 21)
        Me.cbFirebirdServidor2.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.picColor02)
        Me.GroupBox1.Controls.Add(Me.picColor01)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.cboColor02)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cboColor01)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Location = New System.Drawing.Point(0, 284)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1469, 105)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Gerais"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Nome da Cor 02"
        '
        'cboColor02
        '
        Me.cboColor02.FormattingEnabled = True
        Me.cboColor02.Location = New System.Drawing.Point(12, 78)
        Me.cboColor02.Name = "cboColor02"
        Me.cboColor02.Size = New System.Drawing.Size(187, 21)
        Me.cboColor02.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Nome da Cor 01"
        '
        'cboColor01
        '
        Me.cboColor01.FormattingEnabled = True
        Me.cboColor01.Location = New System.Drawing.Point(12, 30)
        Me.cboColor01.Name = "cboColor01"
        Me.cboColor01.Size = New System.Drawing.Size(187, 21)
        Me.cboColor01.TabIndex = 0
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.Color.DarkCyan
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(36, 36)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.ToolStripSeparator1, Me.ToolStripButton2})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1469, 58)
        Me.ToolStrip1.TabIndex = 1
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 58)
        '
        'picColor02
        '
        Me.picColor02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picColor02.Location = New System.Drawing.Point(210, 78)
        Me.picColor02.Name = "picColor02"
        Me.picColor02.Size = New System.Drawing.Size(81, 20)
        Me.picColor02.TabIndex = 5
        Me.picColor02.TabStop = False
        '
        'picColor01
        '
        Me.picColor01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picColor01.Location = New System.Drawing.Point(210, 30)
        Me.picColor01.Name = "picColor01"
        Me.picColor01.Size = New System.Drawing.Size(81, 20)
        Me.picColor01.TabIndex = 4
        Me.picColor01.TabStop = False
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Log_Out_Icon_32
        Me.ToolStripButton1.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(41, 55)
        Me.ToolStripButton1.Text = "&Voltar"
        Me.ToolStripButton1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.ToolStripButton1.ToolTipText = "&Voltar"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Save
        Me.ToolStripButton2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(42, 55)
        Me.ToolStripButton2.Text = "&Salvar"
        Me.ToolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'frmConexoes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1469, 837)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Name = "frmConexoes"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.Text = "Conexões"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.picColor02, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picColor01, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents ToolStripButton1 As ToolStripButton
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txFirebirdPorta As TextBox
    Friend WithEvents Button4 As Button
    Friend WithEvents Button5 As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents txFirebirdSenha As TextBox
    Friend WithEvents txFirebirdUsuario As TextBox
    Friend WithEvents cbFirebirdBanco As ComboBox
    Friend WithEvents cbFirebirdServidor As ComboBox
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents Label21 As Label
    Friend WithEvents txFirebirdPorta2 As TextBox
    Friend WithEvents Button12 As Button
    Friend WithEvents Button14 As Button
    Friend WithEvents Label22 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents txFirebirdSenha2 As TextBox
    Friend WithEvents txFirebirdUsuario2 As TextBox
    Friend WithEvents cbFirebirdBanco2 As ComboBox
    Friend WithEvents cbFirebirdServidor2 As ComboBox
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents ToolStripButton2 As ToolStripButton
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents cboColor01 As ComboBox
    Friend WithEvents ColorDialog1 As ColorDialog
    Friend WithEvents Label2 As Label
    Friend WithEvents cboColor02 As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents picColor02 As PictureBox
    Friend WithEvents picColor01 As PictureBox
End Class
