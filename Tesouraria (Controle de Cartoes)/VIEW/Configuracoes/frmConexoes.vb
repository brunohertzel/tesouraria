﻿Imports System.Data.SqlClient
Imports System.IO
Imports System
Imports System.Windows.Forms
Imports System.Drawing

Public Class frmConexoes

    '===================================================
    '====  LISTAGEM DE INSTANCIAS DO MSSQL SERVER   ====
    '===================================================



    Private Sub frmConexoes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.FormBorderStyle = FormBorderStyle.None
        Me.WindowState = FormWindowState.Maximized
        'Carregar dados do arquivo ini
        abreform(ToolStrip1)

        Dim cores As String() = System.[Enum].GetNames(GetType(System.Drawing.KnownColor))
        cboColor01.Items.AddRange(cores)
        cboColor02.Items.AddRange(cores)

        CARREGAR_INI()

        cbFirebirdServidor.Text = FIREBIRD_SERVIDOR
        txFirebirdUsuario.Text = FIREBIRD_USUARIO
        txFirebirdSenha.Text = FIREBIRD_SENHA
        txFirebirdPorta.Text = FIREBIRD_PORTA
        cbFirebirdBanco.Text = FIREBIRD_BANCO


        cbFirebirdServidor2.Text = FIREBIRD_SERVIDOR2
        txFirebirdUsuario2.Text = FIREBIRD_USUARIO2
        txFirebirdSenha2.Text = FIREBIRD_SENHA2
        txFirebirdPorta2.Text = FIREBIRD_PORTA2
        cbFirebirdBanco2.Text = FIREBIRD_BANCO2


        FIREBIRD_SERVIDOR = ""
        FIREBIRD_USUARIO = ""
        FIREBIRD_SENHA = ""
        FIREBIRD_PORTA = ""
        FIREBIRD_BANCO = ""

        FIREBIRD_SERVIDOR2 = ""
        FIREBIRD_USUARIO2 = ""
        FIREBIRD_SENHA2 = ""
        FIREBIRD_PORTA2 = ""
        FIREBIRD_BANCO2 = ""

        If COLOR01 <> "" Then
            cboColor01.SelectedText = COLOR01
            picColor01.BackColor = Color.FromName(cboColor01.Text)
        Else
            cboColor01.SelectedText = "White"
            picColor01.BackColor = Color.White
        End If

        If COLOR02 <> "" Then
            cboColor02.SelectedText = COLOR02
            picColor02.BackColor = Color.FromName(cboColor02.Text)
        Else
            cboColor02.SelectedText = "Red"
            picColor02.BackColor = Color.Red
        End If


    End Sub




    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        'Gravar dados do Form no Ini

        If cbFirebirdServidor.Text = "" Then
            MessageBox.Show("Informe o Servidor", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
            cbFirebirdServidor.Focus()
            Exit Sub
        End If
        If txFirebirdUsuario.Text = "" Then
            MessageBox.Show("Informe o Usuário", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txFirebirdUsuario.Focus()
            Exit Sub
        End If
        If txFirebirdSenha.Text = "" Then
            MessageBox.Show("Informe a Senha", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txFirebirdSenha.Focus()
            Exit Sub
        End If
        If cbFirebirdBanco.Text = "" Then
            MessageBox.Show("Informe o caminho do banco", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
            cbFirebirdBanco.Focus()
            Exit Sub
        End If
        If txFirebirdPorta.Text = "" Then
            MessageBox.Show("Informe a Porta", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txFirebirdPorta.Focus()
            Exit Sub
        End If

        FIREBIRD_SERVIDOR = cbFirebirdServidor.Text
        FIREBIRD_USUARIO = txFirebirdUsuario.Text
        FIREBIRD_SENHA = txFirebirdSenha.Text()
        FIREBIRD_PORTA = txFirebirdPorta.Text
        FIREBIRD_BANCO = cbFirebirdBanco.Text

        ESCREVER_INI_FIREBIRD()


        FIREBIRD_SERVIDOR = ""
        FIREBIRD_USUARIO = ""
        FIREBIRD_SENHA = ""
        FIREBIRD_PORTA = ""
        FIREBIRD_BANCO = ""
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        OpenFileDialog1.Filter = "Banco de Dados Firebird|*.FDB"
        OpenFileDialog1.FileName = ""
        OpenFileDialog1.ShowDialog()
        cbFirebirdBanco.Text = (OpenFileDialog1.FileName)
    End Sub



    Private Sub Button14_Click(sender As Object, e As EventArgs) Handles Button14.Click
        'Gravar dados do Form no Ini

        If cbFirebirdServidor2.Text = "" Then
            MessageBox.Show("Informe o Servidor", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
            cbFirebirdServidor2.Focus()
            Exit Sub
        End If
        If txFirebirdUsuario2.Text = "" Then
            MessageBox.Show("Informe o Usuário", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txFirebirdUsuario2.Focus()
            Exit Sub
        End If
        If txFirebirdSenha2.Text = "" Then
            MessageBox.Show("Informe a Senha", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txFirebirdSenha2.Focus()
            Exit Sub
        End If
        If cbFirebirdBanco2.Text = "" Then
            MessageBox.Show("Informe o caminho do banco", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
            cbFirebirdBanco2.Focus()
            Exit Sub
        End If
        If txFirebirdPorta2.Text = "" Then
            MessageBox.Show("Informe a Porta", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txFirebirdPorta2.Focus()
            Exit Sub
        End If
        FIREBIRD_SERVIDOR2 = cbFirebirdServidor2.Text
        FIREBIRD_USUARIO2 = txFirebirdUsuario2.Text
        FIREBIRD_SENHA2 = txFirebirdSenha2.Text()
        FIREBIRD_PORTA2 = txFirebirdPorta2.Text
        FIREBIRD_BANCO2 = cbFirebirdBanco2.Text

        ESCREVER_INI_FIREBIRD2()

        FIREBIRD_SERVIDOR2 = ""
        FIREBIRD_USUARIO2 = ""
        FIREBIRD_SENHA2 = ""
        FIREBIRD_PORTA2 = ""
        FIREBIRD_BANCO2 = ""
    End Sub


    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click
        OpenFileDialog1.Filter = "Banco de Dados Firebird|*.FDB"
        OpenFileDialog1.FileName = ""
        OpenFileDialog1.ShowDialog()
        cbFirebirdBanco2.Text = (OpenFileDialog1.FileName)
    End Sub


    Private Sub ToolStripButton1_Click_1(sender As Object, e As EventArgs) Handles ToolStripButton1.Click
        Me.Close()
    End Sub

    Private Sub ToolStripButton2_Click(sender As Object, e As EventArgs) Handles ToolStripButton2.Click
        COLOR01 = cboColor01.Text
        COLOR02 = cboColor02.Text

        If COLOR01 = "" Then
            MsgBox("Cor 01 da Paleta não selecionada!", vbCritical)
            Exit Sub
        ElseIf COLOR02 = "" Then
            MsgBox("Cor 02 da Paleta não selecionada!", vbCritical)
            Exit Sub
        End If

        ESCREVER_INI_GERAL()
    End Sub

    Private Sub cboColor01_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboColor01.SelectedIndexChanged
        picColor01.BackColor = Color.FromName(cboColor01.Text)
    End Sub

    Private Sub cboColor02_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboColor02.SelectedIndexChanged
        picColor02.BackColor = Color.FromName(cboColor02.Text)
    End Sub
End Class