﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmCadastroCartoes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.btnVoltar = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnIncluir = New System.Windows.Forms.ToolStripButton()
        Me.btnEditar = New System.Windows.Forms.ToolStripButton()
        Me.btnSalvar = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.gbxStatus = New System.Windows.Forms.GroupBox()
        Me.rbInativo = New System.Windows.Forms.RadioButton()
        Me.rbAtivo = New System.Windows.Forms.RadioButton()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cbTipoPgto = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.nudDiaFixo = New System.Windows.Forms.NumericUpDown()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cbConciliadora = New System.Windows.Forms.ComboBox()
        Me.gbxSemana = New System.Windows.Forms.GroupBox()
        Me.rbQuarta = New System.Windows.Forms.RadioButton()
        Me.rbDomingo = New System.Windows.Forms.RadioButton()
        Me.rbSabado = New System.Windows.Forms.RadioButton()
        Me.rbQuinta = New System.Windows.Forms.RadioButton()
        Me.rbTerca = New System.Windows.Forms.RadioButton()
        Me.rbDiario = New System.Windows.Forms.RadioButton()
        Me.rbSexta = New System.Windows.Forms.RadioButton()
        Me.rbSegunda = New System.Windows.Forms.RadioButton()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.nudTaxa = New System.Windows.Forms.NumericUpDown()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.nudDiasVcto = New System.Windows.Forms.NumericUpDown()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbTipoCartao = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txNomeCartao = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txCodigo = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbEmpresas = New DevExpress.XtraEditors.LookUpEdit()
        Me.ToolStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.gbxStatus.SuspendLayout()
        CType(Me.nudDiaFixo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxSemana.SuspendLayout()
        CType(Me.nudTaxa, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudDiasVcto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbEmpresas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.SystemColors.ControlDark
        Me.ToolStrip1.GripMargin = New System.Windows.Forms.Padding(3, 3, 4, 3)
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(34, 34)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnVoltar, Me.ToolStripSeparator1, Me.btnIncluir, Me.btnEditar, Me.btnSalvar, Me.ToolStripButton2})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Margin = New System.Windows.Forms.Padding(4)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(987, 56)
        Me.ToolStrip1.TabIndex = 5
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'btnVoltar
        '
        Me.btnVoltar.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Log_Out_Icon_32
        Me.btnVoltar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnVoltar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnVoltar.Name = "btnVoltar"
        Me.btnVoltar.Size = New System.Drawing.Size(41, 53)
        Me.btnVoltar.Text = "&Voltar"
        Me.btnVoltar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnVoltar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 56)
        '
        'btnIncluir
        '
        Me.btnIncluir.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources._1489198437_Add
        Me.btnIncluir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnIncluir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnIncluir.Name = "btnIncluir"
        Me.btnIncluir.Size = New System.Drawing.Size(44, 53)
        Me.btnIncluir.Text = "&Incluir"
        Me.btnIncluir.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnIncluir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnEditar
        '
        Me.btnEditar.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Edit_page
        Me.btnEditar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(41, 53)
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnSalvar
        '
        Me.btnSalvar.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Apply
        Me.btnSalvar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSalvar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSalvar.Name = "btnSalvar"
        Me.btnSalvar.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.btnSalvar.Size = New System.Drawing.Size(42, 53)
        Me.btnSalvar.Text = "&Salvar"
        Me.btnSalvar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Delete
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(57, 53)
        Me.ToolStripButton2.Text = "&Cancelar"
        Me.ToolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.gbxStatus)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.cbTipoPgto)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.nudDiaFixo)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.cbConciliadora)
        Me.GroupBox1.Controls.Add(Me.gbxSemana)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.nudTaxa)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.nudDiasVcto)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.cbTipoCartao)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txNomeCartao)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txCodigo)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cbEmpresas)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Location = New System.Drawing.Point(0, 56)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(987, 203)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Dados do Cartão"
        '
        'gbxStatus
        '
        Me.gbxStatus.Controls.Add(Me.rbInativo)
        Me.gbxStatus.Controls.Add(Me.rbAtivo)
        Me.gbxStatus.Location = New System.Drawing.Point(705, 27)
        Me.gbxStatus.Name = "gbxStatus"
        Me.gbxStatus.Size = New System.Drawing.Size(119, 43)
        Me.gbxStatus.TabIndex = 31
        Me.gbxStatus.TabStop = False
        Me.gbxStatus.Text = "Status"
        '
        'rbInativo
        '
        Me.rbInativo.AutoSize = True
        Me.rbInativo.Location = New System.Drawing.Point(61, 19)
        Me.rbInativo.Name = "rbInativo"
        Me.rbInativo.Size = New System.Drawing.Size(57, 17)
        Me.rbInativo.TabIndex = 1
        Me.rbInativo.Tag = "I"
        Me.rbInativo.Text = "Inativo"
        Me.rbInativo.UseVisualStyleBackColor = True
        '
        'rbAtivo
        '
        Me.rbAtivo.AutoSize = True
        Me.rbAtivo.Checked = True
        Me.rbAtivo.Location = New System.Drawing.Point(6, 19)
        Me.rbAtivo.Name = "rbAtivo"
        Me.rbAtivo.Size = New System.Drawing.Size(49, 17)
        Me.rbAtivo.TabIndex = 0
        Me.rbAtivo.TabStop = True
        Me.rbAtivo.Tag = "A"
        Me.rbAtivo.Text = "Ativo"
        Me.rbAtivo.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(406, 77)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(99, 13)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "Tipo Pagamamento"
        '
        'cbTipoPgto
        '
        Me.cbTipoPgto.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTipoPgto.FormattingEnabled = True
        Me.cbTipoPgto.Items.AddRange(New Object() {"A - Dia da Semana", "B - Dia Fixo", "C - Dia Fixo+Prazo", "D - Dia Fixo+Prazo+Dia da Semana"})
        Me.cbTipoPgto.Location = New System.Drawing.Point(410, 93)
        Me.cbTipoPgto.Name = "cbTipoPgto"
        Me.cbTipoPgto.Size = New System.Drawing.Size(272, 26)
        Me.cbTipoPgto.TabIndex = 17
        Me.cbTipoPgto.Text = "Selecione"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(702, 73)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(45, 13)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Dia Fixo"
        '
        'nudDiaFixo
        '
        Me.nudDiaFixo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nudDiaFixo.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudDiaFixo.Location = New System.Drawing.Point(705, 93)
        Me.nudDiaFixo.Name = "nudDiaFixo"
        Me.nudDiaFixo.Size = New System.Drawing.Size(61, 24)
        Me.nudDiaFixo.TabIndex = 15
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(297, 77)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(93, 13)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Exibir Conciliadora"
        '
        'cbConciliadora
        '
        Me.cbConciliadora.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbConciliadora.FormattingEnabled = True
        Me.cbConciliadora.Items.AddRange(New Object() {"Sim", "Não"})
        Me.cbConciliadora.Location = New System.Drawing.Point(300, 93)
        Me.cbConciliadora.Name = "cbConciliadora"
        Me.cbConciliadora.Size = New System.Drawing.Size(90, 26)
        Me.cbConciliadora.TabIndex = 13
        Me.cbConciliadora.Text = "Selecione"
        '
        'gbxSemana
        '
        Me.gbxSemana.Controls.Add(Me.rbQuarta)
        Me.gbxSemana.Controls.Add(Me.rbDomingo)
        Me.gbxSemana.Controls.Add(Me.rbSabado)
        Me.gbxSemana.Controls.Add(Me.rbQuinta)
        Me.gbxSemana.Controls.Add(Me.rbTerca)
        Me.gbxSemana.Controls.Add(Me.rbDiario)
        Me.gbxSemana.Controls.Add(Me.rbSexta)
        Me.gbxSemana.Controls.Add(Me.rbSegunda)
        Me.gbxSemana.Location = New System.Drawing.Point(12, 125)
        Me.gbxSemana.Name = "gbxSemana"
        Me.gbxSemana.Size = New System.Drawing.Size(400, 66)
        Me.gbxSemana.TabIndex = 12
        Me.gbxSemana.TabStop = False
        Me.gbxSemana.Text = "Dias de Envio"
        '
        'rbQuarta
        '
        Me.rbQuarta.AutoSize = True
        Me.rbQuarta.Location = New System.Drawing.Point(198, 19)
        Me.rbQuarta.Name = "rbQuarta"
        Me.rbQuarta.Size = New System.Drawing.Size(83, 17)
        Me.rbQuarta.TabIndex = 17
        Me.rbQuarta.Tag = "4"
        Me.rbQuarta.Text = "Quarta-Feira"
        Me.rbQuarta.UseVisualStyleBackColor = True
        '
        'rbDomingo
        '
        Me.rbDomingo.AutoSize = True
        Me.rbDomingo.Location = New System.Drawing.Point(198, 42)
        Me.rbDomingo.Name = "rbDomingo"
        Me.rbDomingo.Size = New System.Drawing.Size(67, 17)
        Me.rbDomingo.TabIndex = 18
        Me.rbDomingo.Tag = "1"
        Me.rbDomingo.Text = "Domingo"
        Me.rbDomingo.UseVisualStyleBackColor = True
        '
        'rbSabado
        '
        Me.rbSabado.AutoSize = True
        Me.rbSabado.Location = New System.Drawing.Point(102, 42)
        Me.rbSabado.Name = "rbSabado"
        Me.rbSabado.Size = New System.Drawing.Size(62, 17)
        Me.rbSabado.TabIndex = 16
        Me.rbSabado.Tag = "7"
        Me.rbSabado.Text = "Sábado"
        Me.rbSabado.UseVisualStyleBackColor = True
        '
        'rbQuinta
        '
        Me.rbQuinta.AutoSize = True
        Me.rbQuinta.Location = New System.Drawing.Point(294, 19)
        Me.rbQuinta.Name = "rbQuinta"
        Me.rbQuinta.Size = New System.Drawing.Size(82, 17)
        Me.rbQuinta.TabIndex = 19
        Me.rbQuinta.Tag = "5"
        Me.rbQuinta.Text = "Quinta-Feira"
        Me.rbQuinta.UseVisualStyleBackColor = True
        '
        'rbTerca
        '
        Me.rbTerca.AutoSize = True
        Me.rbTerca.Location = New System.Drawing.Point(102, 19)
        Me.rbTerca.Name = "rbTerca"
        Me.rbTerca.Size = New System.Drawing.Size(79, 17)
        Me.rbTerca.TabIndex = 15
        Me.rbTerca.Tag = "3"
        Me.rbTerca.Text = "Terça-Feira"
        Me.rbTerca.UseVisualStyleBackColor = True
        '
        'rbDiario
        '
        Me.rbDiario.AutoSize = True
        Me.rbDiario.Checked = True
        Me.rbDiario.Location = New System.Drawing.Point(294, 42)
        Me.rbDiario.Name = "rbDiario"
        Me.rbDiario.Size = New System.Drawing.Size(81, 17)
        Me.rbDiario.TabIndex = 20
        Me.rbDiario.TabStop = True
        Me.rbDiario.Tag = "8"
        Me.rbDiario.Text = "Diariamente"
        Me.rbDiario.UseVisualStyleBackColor = True
        '
        'rbSexta
        '
        Me.rbSexta.AutoSize = True
        Me.rbSexta.Location = New System.Drawing.Point(6, 42)
        Me.rbSexta.Name = "rbSexta"
        Me.rbSexta.Size = New System.Drawing.Size(78, 17)
        Me.rbSexta.TabIndex = 1
        Me.rbSexta.Tag = "6"
        Me.rbSexta.Text = "Sexta-Feira"
        Me.rbSexta.UseVisualStyleBackColor = True
        '
        'rbSegunda
        '
        Me.rbSegunda.AutoSize = True
        Me.rbSegunda.Location = New System.Drawing.Point(6, 19)
        Me.rbSegunda.Name = "rbSegunda"
        Me.rbSegunda.Size = New System.Drawing.Size(94, 17)
        Me.rbSegunda.TabIndex = 0
        Me.rbSegunda.Tag = "2"
        Me.rbSegunda.Text = "Segunda-Feira"
        Me.rbSegunda.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(128, 77)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Taxa Adm"
        '
        'nudTaxa
        '
        Me.nudTaxa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nudTaxa.DecimalPlaces = 2
        Me.nudTaxa.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudTaxa.Increment = New Decimal(New Integer() {1, 0, 0, 131072})
        Me.nudTaxa.Location = New System.Drawing.Point(131, 95)
        Me.nudTaxa.Name = "nudTaxa"
        Me.nudTaxa.Size = New System.Drawing.Size(77, 24)
        Me.nudTaxa.TabIndex = 10
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(224, 77)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Dias Vcto"
        '
        'nudDiasVcto
        '
        Me.nudDiasVcto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nudDiasVcto.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudDiasVcto.Location = New System.Drawing.Point(227, 95)
        Me.nudDiasVcto.Name = "nudDiasVcto"
        Me.nudDiasVcto.Size = New System.Drawing.Size(50, 24)
        Me.nudDiasVcto.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(15, 77)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(62, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Tipo Cartão"
        '
        'cbTipoCartao
        '
        Me.cbTipoCartao.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTipoCartao.FormattingEnabled = True
        Me.cbTipoCartao.Items.AddRange(New Object() {"Crédito", "Débito"})
        Me.cbTipoCartao.Location = New System.Drawing.Point(12, 93)
        Me.cbTipoCartao.Name = "cbTipoCartao"
        Me.cbTipoCartao.Size = New System.Drawing.Size(100, 26)
        Me.cbTipoCartao.TabIndex = 6
        Me.cbTipoCartao.Text = "Selecione"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(75, 30)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(69, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Nome Cartão"
        '
        'txNomeCartao
        '
        Me.txNomeCartao.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txNomeCartao.Location = New System.Drawing.Point(78, 46)
        Me.txNomeCartao.Name = "txNomeCartao"
        Me.txNomeCartao.Size = New System.Drawing.Size(236, 24)
        Me.txNomeCartao.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Código"
        '
        'txCodigo
        '
        Me.txCodigo.Enabled = False
        Me.txCodigo.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txCodigo.Location = New System.Drawing.Point(12, 46)
        Me.txCodigo.Name = "txCodigo"
        Me.txCodigo.ReadOnly = True
        Me.txCodigo.Size = New System.Drawing.Size(59, 24)
        Me.txCodigo.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(327, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Empresa"
        '
        'cbEmpresas
        '
        Me.cbEmpresas.Location = New System.Drawing.Point(330, 46)
        Me.cbEmpresas.Name = "cbEmpresas"
        Me.cbEmpresas.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbEmpresas.Properties.Appearance.Options.UseFont = True
        Me.cbEmpresas.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.cbEmpresas.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbEmpresas.Size = New System.Drawing.Size(352, 24)
        Me.cbEmpresas.TabIndex = 0
        '
        'frmCadastroCartoes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(987, 634)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Name = "frmCadastroCartoes"
        Me.Text = "Cadastro de Cartões"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.gbxStatus.ResumeLayout(False)
        Me.gbxStatus.PerformLayout()
        CType(Me.nudDiaFixo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxSemana.ResumeLayout(False)
        Me.gbxSemana.PerformLayout()
        CType(Me.nudTaxa, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudDiasVcto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbEmpresas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents btnVoltar As ToolStripButton
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents btnIncluir As ToolStripButton
    Friend WithEvents btnEditar As ToolStripButton
    Friend WithEvents btnSalvar As ToolStripButton
    Friend WithEvents ToolStripButton2 As ToolStripButton
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents cbEmpresas As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label1 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents cbTipoPgto As ComboBox
    Friend WithEvents Label8 As Label
    Friend WithEvents nudDiaFixo As NumericUpDown
    Friend WithEvents Label7 As Label
    Friend WithEvents cbConciliadora As ComboBox
    Friend WithEvents gbxSemana As GroupBox
    Friend WithEvents rbQuarta As RadioButton
    Friend WithEvents rbDomingo As RadioButton
    Friend WithEvents rbSabado As RadioButton
    Friend WithEvents rbQuinta As RadioButton
    Friend WithEvents rbTerca As RadioButton
    Friend WithEvents rbDiario As RadioButton
    Friend WithEvents rbSexta As RadioButton
    Friend WithEvents rbSegunda As RadioButton
    Friend WithEvents Label6 As Label
    Friend WithEvents nudTaxa As NumericUpDown
    Friend WithEvents Label5 As Label
    Friend WithEvents nudDiasVcto As NumericUpDown
    Friend WithEvents Label4 As Label
    Friend WithEvents cbTipoCartao As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txNomeCartao As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txCodigo As TextBox
    Friend WithEvents gbxStatus As GroupBox
    Friend WithEvents rbInativo As RadioButton
    Friend WithEvents rbAtivo As RadioButton
End Class
