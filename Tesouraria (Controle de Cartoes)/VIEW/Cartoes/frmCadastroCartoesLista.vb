﻿Imports System.Drawing.Drawing2D

Public Class frmCadastroCartoesLista

    'Botão Saida da tela
    Private Sub btnVoltar_Click(sender As Object, e As EventArgs) Handles btnVoltar.Click
        Me.Close()
    End Sub

    Private Sub btnLocalizar_Click(sender As Object, e As EventArgs) Handles btnLocalizar.Click
        Dim consultas As New cls_Cartoes
        Dim ctlCartoes As New ctr_Cartoes
        If rbAtivo.Checked = True Then
            consultas.Status = "I"
        ElseIf rbInativo.Checked = True Then
            consultas.Status = "A"
        End If
        ctlCartoes.Consulta(consultas, Me)
    End Sub

    'Eventos de Abertura do Form
    Private Sub frmCadastroCartoesLista_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.FormBorderStyle = FormBorderStyle.None
        Me.WindowState = FormWindowState.Maximized
        CARREGAR_INI()
        abreform(ToolStrip1)
    End Sub

    'Evento do Botao Incluir
    Private Sub btnIncluir_Click(sender As Object, e As EventArgs) Handles btnIncluir.Click
        ENTRADA_DADOS = "I"
        frmCadastroCartoes.MdiParent = frmMenuPrincipal
        frmCadastroCartoes.Show()
    End Sub

    Private Sub gcCartoes_DoubleClick(sender As Object, e As EventArgs) Handles gcCartoes.DoubleClick
        Dim ID As String = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), gCartaoCod)
        Dim editar As New ctr_Cartoes
        Dim cartao As New cls_CartloesInclusao
        cartao.codigo = ID
        editar.EditaCartoes(cartao)
    End Sub


End Class