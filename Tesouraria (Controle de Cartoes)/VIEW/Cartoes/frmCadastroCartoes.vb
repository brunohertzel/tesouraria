﻿Public Class frmCadastroCartoes

    'Eventos de abertura do Form
    Private Sub frmCadastroCartoes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim consulta As New ctr_Empresas
        consulta.consultaEmpresas(cbEmpresas)
    End Sub

    Private Sub btnVoltar_Click(sender As Object, e As EventArgs) Handles btnVoltar.Click
        Me.Close()
    End Sub

    Private Sub btnSalvar_Click(sender As Object, e As EventArgs) Handles btnSalvar.Click
        Dim cartoes As New cls_CartloesInclusao
        Dim dadosCartaoAtual As New cls_CartloesInclusao
        dadosCartaoAtual.codigo = txCodigo.Text
        cartoes.nome = txNomeCartao.Text
        dadosCartaoAtual.nome = txNomeCartao.Text
        cartoes.empresa = cbEmpresas.EditValue
        dadosCartaoAtual.empresa = cbEmpresas.EditValue
        cartoes.tipo = cbTipoCartao.SelectedIndex
        dadosCartaoAtual.tipo = cbTipoCartao.SelectedIndex
        cartoes.Taxa = nudTaxa.Value.ToString.Trim()
        dadosCartaoAtual.Taxa = nudTaxa.Value.ToString.Trim()
        cartoes.diasVencimento = nudDiasVcto.Value.ToString.Trim()
        dadosCartaoAtual.diasVencimento = nudDiasVcto.Value.ToString.Trim()
        cartoes.conciliadora = cbConciliadora.SelectedIndex
        dadosCartaoAtual.conciliadora = cbConciliadora.SelectedIndex
        cartoes.tipoPgto = cbTipoPgto.SelectedIndex
        dadosCartaoAtual.tipoPgto = cbTipoPgto.SelectedIndex
        cartoes.diaFixo = nudDiaFixo.Value.ToString.Trim()
        dadosCartaoAtual.diaFixo = nudDiaFixo.Value.ToString.Trim()
        For Each ctrl As Control In gbxSemana.Controls
            If ctrl.GetType().ToString = "System.Windows.Forms.RadioButton" Then
                If (CType(ctrl, RadioButton)).Checked = True Then
                    cartoes.diaEnvio = (CType(ctrl, RadioButton)).Tag
                    dadosCartaoAtual.diaEnvio = (CType(ctrl, RadioButton)).Tag
                End If
            End If
        Next
        For Each ctrlStatus As Control In gbxStatus.Controls
            If ctrlStatus.GetType().ToString = "System.Windows.Forms.RadioButton" Then
                If (CType(ctrlStatus, RadioButton)).Checked = True Then
                    cartoes.status = (CType(ctrlStatus, RadioButton)).Tag
                    dadosCartaoAtual.status = (CType(ctrlStatus, RadioButton)).Tag
                End If
            End If
        Next
        Dim ctrCartoes As New ctr_Cartoes
        ctrCartoes.Incluir_Alterar(cartoes, Me)
        'teste


    End Sub

    Private Sub cbTipoPgto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTipoPgto.SelectedIndexChanged
        If cbTipoPgto.SelectedIndex = 0 Then
            nudDiaFixo.Enabled = False
            nudDiasVcto.Enabled = True
            gbxSemana.Enabled = True
        ElseIf cbTipoPgto.SelectedIndex = 1 Then
            nudDiaFixo.Enabled = True
            nudDiasVcto.Enabled = False
            gbxSemana.Enabled = False
        ElseIf cbTipoPgto.SelectedIndex = 2 Then
            nudDiaFixo.Enabled = True
            nudDiasVcto.Enabled = True
            gbxSemana.Enabled = False
        ElseIf cbTipoPgto.SelectedIndex = 3 Then
            nudDiaFixo.Enabled = True
            nudDiasVcto.Enabled = True
            gbxSemana.Enabled = True
        End If
    End Sub
End Class