﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCadastroCartoesLista
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.btnVoltar = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnIncluir = New System.Windows.Forms.ToolStripButton()
        Me.btnEditar = New System.Windows.Forms.ToolStripButton()
        Me.btnGrade = New System.Windows.Forms.ToolStripSplitButton()
        Me.btnSalvarGrid = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExcluirGrid = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnExportarExcel = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExportarHTML = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExportarPDF = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExportarTexto = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rbInativo = New System.Windows.Forms.RadioButton()
        Me.rbAtivo = New System.Windows.Forms.RadioButton()
        Me.lbPesquisa = New System.Windows.Forms.Label()
        Me.btnLocalizar = New System.Windows.Forms.Button()
        Me.txBusca = New System.Windows.Forms.TextBox()
        Me.gcCartoes = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gCartaoCod = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gCartaoDescricao = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gTaxa = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gTipoCartao = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gConciliadora = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gPrazo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gDiaSemana = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gEmpresaID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gEmpresaNome = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gEmpresaFantasia = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gStatus = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.ToolStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.gcCartoes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.Color.SteelBlue
        Me.ToolStrip1.GripMargin = New System.Windows.Forms.Padding(3, 3, 4, 3)
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(34, 34)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnVoltar, Me.ToolStripSeparator1, Me.btnIncluir, Me.btnEditar, Me.btnGrade})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Margin = New System.Windows.Forms.Padding(4)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1165, 56)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'btnVoltar
        '
        Me.btnVoltar.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Log_Out_Icon_32
        Me.btnVoltar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnVoltar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnVoltar.Name = "btnVoltar"
        Me.btnVoltar.Size = New System.Drawing.Size(41, 53)
        Me.btnVoltar.Text = "&Voltar"
        Me.btnVoltar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnVoltar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 56)
        '
        'btnIncluir
        '
        Me.btnIncluir.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources._1489198437_Add
        Me.btnIncluir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnIncluir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnIncluir.Name = "btnIncluir"
        Me.btnIncluir.Size = New System.Drawing.Size(44, 53)
        Me.btnIncluir.Text = "&Incluir"
        Me.btnIncluir.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnIncluir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnEditar
        '
        Me.btnEditar.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Edit_page
        Me.btnEditar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(41, 53)
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnGrade
        '
        Me.btnGrade.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnSalvarGrid, Me.btnExcluirGrid, Me.ToolStripSeparator2, Me.btnExportarExcel, Me.btnExportarHTML, Me.btnExportarPDF, Me.btnExportarTexto})
        Me.btnGrade.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.salva_grade48
        Me.btnGrade.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnGrade.Name = "btnGrade"
        Me.btnGrade.Size = New System.Drawing.Size(54, 53)
        Me.btnGrade.Text = "Grade"
        Me.btnGrade.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnSalvarGrid
        '
        Me.btnSalvarGrid.Name = "btnSalvarGrid"
        Me.btnSalvarGrid.Size = New System.Drawing.Size(153, 22)
        Me.btnSalvarGrid.Text = "Salvar Grade"
        '
        'btnExcluirGrid
        '
        Me.btnExcluirGrid.Name = "btnExcluirGrid"
        Me.btnExcluirGrid.Size = New System.Drawing.Size(153, 22)
        Me.btnExcluirGrid.Text = "Excluir Grade"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(150, 6)
        '
        'btnExportarExcel
        '
        Me.btnExportarExcel.Name = "btnExportarExcel"
        Me.btnExportarExcel.Size = New System.Drawing.Size(153, 22)
        Me.btnExportarExcel.Text = "Exportar Excel"
        '
        'btnExportarHTML
        '
        Me.btnExportarHTML.Name = "btnExportarHTML"
        Me.btnExportarHTML.Size = New System.Drawing.Size(153, 22)
        Me.btnExportarHTML.Text = "Exportar HTML"
        '
        'btnExportarPDF
        '
        Me.btnExportarPDF.Name = "btnExportarPDF"
        Me.btnExportarPDF.Size = New System.Drawing.Size(153, 22)
        Me.btnExportarPDF.Text = "Exportar PDF"
        '
        'btnExportarTexto
        '
        Me.btnExportarTexto.Name = "btnExportarTexto"
        Me.btnExportarTexto.Size = New System.Drawing.Size(153, 22)
        Me.btnExportarTexto.Text = "Exportar Texto"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.lbPesquisa)
        Me.GroupBox1.Controls.Add(Me.btnLocalizar)
        Me.GroupBox1.Controls.Add(Me.txBusca)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Location = New System.Drawing.Point(0, 56)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1165, 71)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Cadastro de Cartões"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rbInativo)
        Me.GroupBox2.Controls.Add(Me.rbAtivo)
        Me.GroupBox2.Location = New System.Drawing.Point(451, 7)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(136, 58)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Status"
        '
        'rbInativo
        '
        Me.rbInativo.AutoSize = True
        Me.rbInativo.Location = New System.Drawing.Point(66, 24)
        Me.rbInativo.Name = "rbInativo"
        Me.rbInativo.Size = New System.Drawing.Size(62, 17)
        Me.rbInativo.TabIndex = 1
        Me.rbInativo.Text = "Inativos"
        Me.rbInativo.UseVisualStyleBackColor = True
        '
        'rbAtivo
        '
        Me.rbAtivo.AutoSize = True
        Me.rbAtivo.Checked = True
        Me.rbAtivo.Location = New System.Drawing.Point(6, 24)
        Me.rbAtivo.Name = "rbAtivo"
        Me.rbAtivo.Size = New System.Drawing.Size(54, 17)
        Me.rbAtivo.TabIndex = 0
        Me.rbAtivo.TabStop = True
        Me.rbAtivo.Text = "Ativos"
        Me.rbAtivo.UseVisualStyleBackColor = True
        '
        'lbPesquisa
        '
        Me.lbPesquisa.AutoSize = True
        Me.lbPesquisa.ForeColor = System.Drawing.Color.DimGray
        Me.lbPesquisa.Location = New System.Drawing.Point(6, 16)
        Me.lbPesquisa.Name = "lbPesquisa"
        Me.lbPesquisa.Size = New System.Drawing.Size(238, 13)
        Me.lbPesquisa.TabIndex = 2
        Me.lbPesquisa.Text = "Digite o Código ou a descrição a ser pesquisado."
        '
        'btnLocalizar
        '
        Me.btnLocalizar.Image = Global.Tesouraria__Controle_de_Cartoes_.My.Resources.Resources.Search_icon
        Me.btnLocalizar.Location = New System.Drawing.Point(372, 31)
        Me.btnLocalizar.Name = "btnLocalizar"
        Me.btnLocalizar.Size = New System.Drawing.Size(44, 31)
        Me.btnLocalizar.TabIndex = 1
        Me.btnLocalizar.UseVisualStyleBackColor = True
        '
        'txBusca
        '
        Me.txBusca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txBusca.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txBusca.Location = New System.Drawing.Point(6, 31)
        Me.txBusca.Name = "txBusca"
        Me.txBusca.Size = New System.Drawing.Size(360, 31)
        Me.txBusca.TabIndex = 0
        '
        'gcCartoes
        '
        Me.gcCartoes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcCartoes.Location = New System.Drawing.Point(0, 127)
        Me.gcCartoes.MainView = Me.GridView1
        Me.gcCartoes.Name = "gcCartoes"
        Me.gcCartoes.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemComboBox1})
        Me.gcCartoes.Size = New System.Drawing.Size(1165, 506)
        Me.gcCartoes.TabIndex = 2
        Me.gcCartoes.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gCartaoCod, Me.gCartaoDescricao, Me.gTaxa, Me.gTipoCartao, Me.gConciliadora, Me.gPrazo, Me.gDiaSemana, Me.gEmpresaID, Me.gEmpresaNome, Me.gEmpresaFantasia, Me.gStatus})
        Me.GridView1.GridControl = Me.gcCartoes
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        '
        'gCartaoCod
        '
        Me.gCartaoCod.Caption = "Código Cartão"
        Me.gCartaoCod.FieldName = "CARTAO_ID"
        Me.gCartaoCod.Name = "gCartaoCod"
        Me.gCartaoCod.OptionsColumn.AllowEdit = False
        Me.gCartaoCod.OptionsColumn.ReadOnly = True
        Me.gCartaoCod.Visible = True
        Me.gCartaoCod.VisibleIndex = 0
        Me.gCartaoCod.Width = 85
        '
        'gCartaoDescricao
        '
        Me.gCartaoDescricao.Caption = "Descrição do Cartão"
        Me.gCartaoDescricao.FieldName = "CARTAO_NOME"
        Me.gCartaoDescricao.Name = "gCartaoDescricao"
        Me.gCartaoDescricao.OptionsColumn.AllowEdit = False
        Me.gCartaoDescricao.OptionsColumn.ReadOnly = True
        Me.gCartaoDescricao.Visible = True
        Me.gCartaoDescricao.VisibleIndex = 1
        Me.gCartaoDescricao.Width = 285
        '
        'gTaxa
        '
        Me.gTaxa.Caption = "Taxa Administrativa"
        Me.gTaxa.DisplayFormat.FormatString = "n2"
        Me.gTaxa.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gTaxa.FieldName = "CARTAO_TAXA"
        Me.gTaxa.Name = "gTaxa"
        Me.gTaxa.OptionsColumn.AllowEdit = False
        Me.gTaxa.OptionsColumn.ReadOnly = True
        Me.gTaxa.Visible = True
        Me.gTaxa.VisibleIndex = 2
        Me.gTaxa.Width = 103
        '
        'gTipoCartao
        '
        Me.gTipoCartao.Caption = "Tipo Cartão"
        Me.gTipoCartao.FieldName = "CARTAO_TIPO"
        Me.gTipoCartao.Name = "gTipoCartao"
        Me.gTipoCartao.OptionsColumn.AllowEdit = False
        Me.gTipoCartao.OptionsColumn.ReadOnly = True
        Me.gTipoCartao.Visible = True
        Me.gTipoCartao.VisibleIndex = 3
        Me.gTipoCartao.Width = 71
        '
        'gConciliadora
        '
        Me.gConciliadora.Caption = "Integra Conciliadora"
        Me.gConciliadora.FieldName = "CARTAO_CONCILIADORA"
        Me.gConciliadora.Name = "gConciliadora"
        Me.gConciliadora.OptionsColumn.AllowEdit = False
        Me.gConciliadora.OptionsColumn.ReadOnly = True
        Me.gConciliadora.Visible = True
        Me.gConciliadora.VisibleIndex = 4
        Me.gConciliadora.Width = 106
        '
        'gPrazo
        '
        Me.gPrazo.Caption = "Dias Vencimento"
        Me.gPrazo.FieldName = "CARTAO_DIAS_VENCIMENTO"
        Me.gPrazo.Name = "gPrazo"
        Me.gPrazo.OptionsColumn.AllowEdit = False
        Me.gPrazo.OptionsColumn.ReadOnly = True
        Me.gPrazo.Visible = True
        Me.gPrazo.VisibleIndex = 5
        Me.gPrazo.Width = 92
        '
        'gDiaSemana
        '
        Me.gDiaSemana.Caption = "Dias de Envio (Semana)"
        Me.gDiaSemana.FieldName = "CARTAO_DIA_ENVIO"
        Me.gDiaSemana.Name = "gDiaSemana"
        Me.gDiaSemana.OptionsColumn.AllowEdit = False
        Me.gDiaSemana.OptionsColumn.ReadOnly = True
        Me.gDiaSemana.Visible = True
        Me.gDiaSemana.VisibleIndex = 6
        Me.gDiaSemana.Width = 130
        '
        'gEmpresaID
        '
        Me.gEmpresaID.Caption = "Código Empresa"
        Me.gEmpresaID.FieldName = "EMPRESA_ID"
        Me.gEmpresaID.Name = "gEmpresaID"
        Me.gEmpresaID.OptionsColumn.AllowEdit = False
        Me.gEmpresaID.OptionsColumn.ReadOnly = True
        Me.gEmpresaID.Visible = True
        Me.gEmpresaID.VisibleIndex = 7
        Me.gEmpresaID.Width = 91
        '
        'gEmpresaNome
        '
        Me.gEmpresaNome.Caption = "Empresa"
        Me.gEmpresaNome.FieldName = "EMPRESA_NOME"
        Me.gEmpresaNome.Name = "gEmpresaNome"
        Me.gEmpresaNome.OptionsColumn.AllowEdit = False
        Me.gEmpresaNome.OptionsColumn.ReadOnly = True
        Me.gEmpresaNome.Visible = True
        Me.gEmpresaNome.VisibleIndex = 8
        Me.gEmpresaNome.Width = 250
        '
        'gEmpresaFantasia
        '
        Me.gEmpresaFantasia.Caption = "Nome Fantasia"
        Me.gEmpresaFantasia.FieldName = "EMPRESA_NOME_FANTASIA"
        Me.gEmpresaFantasia.Name = "gEmpresaFantasia"
        Me.gEmpresaFantasia.OptionsColumn.AllowEdit = False
        Me.gEmpresaFantasia.OptionsColumn.ReadOnly = True
        Me.gEmpresaFantasia.Visible = True
        Me.gEmpresaFantasia.VisibleIndex = 9
        Me.gEmpresaFantasia.Width = 250
        '
        'gStatus
        '
        Me.gStatus.Caption = "Status Cartão"
        Me.gStatus.FieldName = "CARTAO_STATUS"
        Me.gStatus.Name = "gStatus"
        Me.gStatus.OptionsColumn.AllowEdit = False
        Me.gStatus.OptionsColumn.ReadOnly = True
        Me.gStatus.Visible = True
        Me.gStatus.VisibleIndex = 10
        Me.gStatus.Width = 80
        '
        'RepositoryItemComboBox1
        '
        Me.RepositoryItemComboBox1.AutoHeight = False
        Me.RepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox1.Name = "RepositoryItemComboBox1"
        '
        'frmCadastroCartoesLista
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1165, 633)
        Me.Controls.Add(Me.gcCartoes)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Name = "frmCadastroCartoesLista"
        Me.Text = "Listagem de Cartões"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.gcCartoes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents btnVoltar As ToolStripButton
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents btnIncluir As ToolStripButton
    Friend WithEvents btnEditar As ToolStripButton
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents gcCartoes As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txBusca As TextBox
    Friend WithEvents btnLocalizar As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents rbInativo As RadioButton
    Friend WithEvents rbAtivo As RadioButton
    Friend WithEvents lbPesquisa As Label
    Friend WithEvents btnGrade As ToolStripSplitButton
    Friend WithEvents btnSalvarGrid As ToolStripMenuItem
    Friend WithEvents btnExcluirGrid As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents btnExportarExcel As ToolStripMenuItem
    Friend WithEvents btnExportarHTML As ToolStripMenuItem
    Friend WithEvents btnExportarPDF As ToolStripMenuItem
    Friend WithEvents btnExportarTexto As ToolStripMenuItem
    Friend WithEvents gCartaoCod As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gCartaoDescricao As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gTaxa As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gTipoCartao As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gConciliadora As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gPrazo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gDiaSemana As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gEmpresaID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gEmpresaNome As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gEmpresaFantasia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gStatus As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
End Class
