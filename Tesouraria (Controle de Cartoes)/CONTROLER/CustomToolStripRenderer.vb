﻿Imports System.Drawing.Drawing2D

Public Class CustomToolStripRenderer
    Inherits ToolStripProfessionalRenderer
    Public Sub New()
    End Sub

    Protected Overrides Sub OnRenderToolStripBackground(ByVal e As ToolStripRenderEventArgs)
        CARREGAR_INI()
        If COLOR01 = "" Then
            COLOR01 = "White"
        End If
        If COLOR02 = "" Then
            COLOR02 = "Red"
        End If

        Dim mode As LinearGradientMode = LinearGradientMode.Horizontal
        Using b As LinearGradientBrush = New LinearGradientBrush(e.AffectedBounds, Color.FromName(COLOR01), Color.FromName(COLOR02), mode)
            e.Graphics.FillRectangle(b, e.AffectedBounds)
        End Using
    End Sub

End Class
