﻿Public Class ctr_Empresas

    Public Function consultaEmpresas(combo As DevExpress.XtraEditors.LookUpEdit)
        Dim consulta As New daoEmpresas()
        consulta.daoConsultaEmpresa()
        With combo
            .Properties.DataSource = fs.Tables(TABELA)
            .Properties.DisplayMember = "EMPRESA_NOME"
            .Properties.ValueMember = "EMPRESA_ID"
            .Properties.NullText = "Selecione a Empresa"
        End With
        Return True
    End Function


End Class
