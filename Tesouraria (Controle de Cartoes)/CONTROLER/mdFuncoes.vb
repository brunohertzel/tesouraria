﻿Imports System.Threading
Imports System.IO
Imports System.Net
Imports System.Security.Cryptography

Module mdFuncoes

    Public USUARIO_LOGADO_ID As String
    Public USUARIO_LOGADO_NOME As String
    Public ALTERACOES As String

    Public Function SoNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890", Chr(Keyascii)) = 0 Then
            SoNumeros = 0
        Else
            SoNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoNumeros = Keyascii
            Case 13
                SoNumeros = Keyascii
            Case 32
                SoNumeros = Keyascii
        End Select
    End Function


    Function testeConexao() As Boolean
        SQL = "select * from empresas"
        TABELA = "TABELA"
        Try
            CONEXAO_FIREBIRD(SQL)
            If fs.Tables(TABELA).Rows.Count > 0 Then
                Return True
            End If
        Catch
            MsgBox("Erro de Conexão!")
        End Try
        Return False
    End Function


    Sub abreform(ByVal J As ToolStrip)
        Dim r As CustomToolStripRenderer = New CustomToolStripRenderer()
        r.RoundedEdges = False
        J.Renderer = r
    End Sub

    Sub BuscaCep(ByVal CEP As String, ByVal logradouro As TextBox, ByVal cidade As TextBox, ByVal UF As TextBox, ByVal bairro As TextBox)
        Dim dsCEP As New DataSet()
        Dim xml As String = "http://viacep.com.br/ws/@cep/xml/".Replace("@cep", Replace(CEP, ".", ""))
        dsCEP.ReadXml(xml)
        logradouro.Text = dsCEP.Tables(0).Rows(0)("logradouro").ToString
        cidade.Text = dsCEP.Tables(0).Rows(0)("localidade").ToString
        UF.Text = dsCEP.Tables(0).Rows(0)("UF").ToString
        bairro.Text = dsCEP.Tables(0).Rows(0)("bairro").ToString
    End Sub

    Public Function ConverteImagemParaByteArray(ByVal img As Image) As Byte()
        Try
            Using mStream As MemoryStream = New MemoryStream()
                img.Save(mStream, img.RawFormat)
                Return mStream.ToArray()
            End Using
        Catch __unusedException1__ As Exception
            Throw
        End Try
    End Function

    Function UnicodeBytesToString(
    ByVal bytes() As Byte) As String
        Return System.Text.Encoding.UTF8.GetString(bytes)
    End Function

    Public logDado, logTipo, gerarLog As String
    Sub GravarLog()
        If gerarLog = 1 Then

            SQL = "INSERT INTO LOGS (LOG_DATA, LOG_HORA, LOG_USUARIO, LOG_DESCRICAO, LOG_IP_ORIGEM, LOG_ESTACAO, LOG_TIPO)
                VALUES ('" & Format(Now, "yyyy.MM.dd") & "',
                        '" & Format(Now, "HH:mm:ss") & "',
                          " & USUARIO_LOGADO_ID & ",
                        '" & logDado & "',
                        '" & GetIpV4() & "',
                        '" & GetHostName() & "',
                        '" & logTipo & "')"
            TABELA = "TABELA"
            CONEXAO_FIREBIRD(SQL)
        End If
    End Sub

    Public Function GetHostName() As String
        Dim shostname As String
        shostname = System.Net.Dns.GetHostName
        GetHostName = shostname
    End Function

    Public Function GetIpV4() As String

        Dim myHost As String = Dns.GetHostName
        Dim ipEntry As IPHostEntry = Dns.GetHostEntry(myHost)
        Dim ip As String = ""

        For Each tmpIpAddress As IPAddress In ipEntry.AddressList
            If tmpIpAddress.AddressFamily = Sockets.AddressFamily.InterNetwork Then
                Dim ipAddress As String = tmpIpAddress.ToString
                ip = ipAddress
                Exit For
            End If
        Next
        If ip = "" Then
            Throw New Exception("No 10. IP found!")
        End If
        Return ip
    End Function

    Public Function Crypt(Text As String) As String

        Dim strTempChar As String = ""
        For i = 1 To Len(Text)
            If Asc(Mid$(Text, i, 1)) < 128 Then
                strTempChar = Asc(Mid$(Text, i, 1)) + 128
            ElseIf Asc(Mid$(Text, i, 1)) > 128 Then
                strTempChar = Asc(Mid$(Text, i, 1)) - 128
            End If
            Mid$(Text, i, 1) = Chr(strTempChar)
        Next i
        Crypt = Text
    End Function

    Public Sub Limpar(ByVal controlP As Control)
        Dim ctl As Control
        For Each ctl In controlP.Controls
            If TypeOf ctl Is TextBox Then
                DirectCast(ctl, TextBox).Text = String.Empty
            ElseIf ctl.Controls.Count > 0 Then
                Limpar(ctl)
            End If

            If TypeOf ctl Is MaskedTextBox Then
                DirectCast(ctl, MaskedTextBox).Text = String.Empty
            ElseIf ctl.Controls.Count > 0 Then
                Limpar(ctl)
            End If

        Next
    End Sub


    Public Function ConverteByteParaImagem(ByVal arquivo As Byte()) As Image
        Try
            Using mStream As MemoryStream = New MemoryStream(arquivo)
                Return Image.FromStream(mStream)
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Function

    Function VERIFICA_LOG(ByVal DADO_LOG As String, ByVal DADO_ORIGINAL As String, ByVal DADO_ALTERADO As Object)
        If DADO_ORIGINAL <> DADO_ALTERADO Then
            logDado += DADO_LOG & " de " & DADO_ORIGINAL & " para " & DADO_ALTERADO
            ALTERACOES = "S"
        End If
        Return True
    End Function


    Function salvarGrid(ByVal grid As DevExpress.XtraGrid.GridControl, ByVal nomeGrid As String)
        Dim filename As String
        filename = "GRID\" & nomeGrid & ".xml"
        grid.MainView.SaveLayoutToXml(filename)
        MsgBox("Grade Salva com Sucesso!")
        Return True
    End Function

    Function carregarGrid(ByVal grid As DevExpress.XtraGrid.GridControl, ByVal nomeGrid As String)
        Dim filename As String = "GRID\" & nomeGrid & ".xml"
        If System.IO.Directory.Exists("./GRID") = False Then
            System.IO.Directory.CreateDirectory("./GRID")
        End If
        If System.IO.File.Exists(filename) Then
            grid.MainView.RestoreLayoutFromXml(filename)
        End If
        Return True
    End Function

    'Exportar para Excel
    Public Sub exportarExcel(ByVal grid As DevExpress.XtraGrid.GridControl, ByVal Nome As String)
        Dim save As New SaveFileDialog
        Dim arquivo As String
        save.FileName = Nome & ".xlsx"
        save.Filter = "Excel (XLSX)|*.xlsx"
        save.ShowDialog()
        arquivo = save.FileName
        If save.ShowDialog() = DialogResult.Cancel Then
            Exit Sub
        Else
            grid.ExportToXlsx(arquivo)
            MsgBox("Arquivo " & arquivo & " exportado com Sucesso!", vbInformation)
        End If
    End Sub

    'Exportar para HTML
    Public Sub exportarHTML(ByVal grid As DevExpress.XtraGrid.GridControl, ByVal Nome As String)
        Dim save As New SaveFileDialog
        Dim arquivo As String
        save.FileName = Nome & ".html"
        save.Filter = "Documento HTML (HTML)|*.html"
        save.ShowDialog()
        arquivo = save.FileName
        If save.ShowDialog() = DialogResult.Cancel Then
            Exit Sub
        Else
            grid.ExportToHtml(arquivo)
            MsgBox("Arquivo " & arquivo & " exportado com Sucesso!", vbInformation)
        End If
    End Sub

    'Exportar para PDF
    Public Sub exportarPDF(ByVal grid As DevExpress.XtraGrid.GridControl, ByVal Nome As String)
        Dim save As New SaveFileDialog
        Dim arquivo As String
        save.FileName = Nome & ".pdf"
        save.Filter = "Portable Document Format (PDF)|*.pdf"
        save.ShowDialog()
        arquivo = save.FileName
        If save.ShowDialog() = DialogResult.Cancel Then
            Exit Sub
        Else
            grid.ExportToPdf(arquivo)
            MsgBox("Arquivo " & arquivo & " exportado com Sucesso!", vbInformation)
        End If
    End Sub

    'Exportar para Texto
    Public Sub exportarTexto(ByVal grid As DevExpress.XtraGrid.GridControl, ByVal Nome As String)
        Dim save As New SaveFileDialog
        Dim arquivo As String
        save.FileName = Nome & ".txt"
        save.Filter = "Documento de Texto (txt)|*.txt"
        save.ShowDialog()
        arquivo = save.FileName
        If save.ShowDialog() = DialogResult.Cancel Then
            Exit Sub
        Else
            grid.ExportToPdf(arquivo)
            MsgBox("Arquivo " & arquivo & " exportado com Sucesso!", vbInformation)
        End If
    End Sub


End Module
