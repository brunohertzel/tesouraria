﻿Imports System.Text
Imports System.Security.Cryptography

Public Class cls_crypto

    Private Shared TripleDES As New TripleDESCryptoServiceProvider
    Private Shared MD5 As New MD5CryptoServiceProvider

    ' Definição da chave de encriptação/desencriptação 
    Private Const key As String = "SOFTAUTOMACAO"

    ''' <summary> 
    ''' Calcula o MD5 Hash  
    ''' </summary> 
    ''' <param name="value">Chave</param> 
    Public Shared Function MD5Hash(ByVal value As String) As Byte()
        ' Converte a chave para um array de bytes  
        Dim byteArray() As Byte = ASCIIEncoding.UTF8.GetBytes(value)
        Return MD5.ComputeHash(byteArray)

    End Function

    ''' <summary> 
    ''' Encripta uma string com base em uma chave 
    ''' </summary> 
    ''' <param name="stringToEncrypt">String a encriptar</param> 
    Public Function Encrypt(ByVal stringToEncrypt As String) As String

        Try
            ' Definição da chave e da cifra (que neste caso é Electronic 
            ' Codebook, ou seja, encriptação individual para cada bloco) 
            TripleDES.Key = cls_crypto.MD5Hash(key)
            TripleDES.Mode = CipherMode.ECB

            ' Converte a string para bytes e encripta 
            Dim Buffer As Byte() = ASCIIEncoding.ASCII.GetBytes(stringToEncrypt)
            Return Convert.ToBase64String _
                (TripleDES.CreateEncryptor().TransformFinalBlock(Buffer, 0, Buffer.Length))

        Catch ex As Exception

            MessageBox.Show(ex.Message, My.Application.Info.Title, _
                            MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return String.Empty
        End Try

    End Function

    ''' <summary> 
    ''' Desencripta uma string com base em uma chave 
    ''' </summary> 
    ''' <param name="encryptedString">String a decriptar</param> 
    Public Function Decrypt(ByVal encryptedString As String) As String

        Try
            ' Definição da chave e da cifra 
            If encryptedString = String.Empty Then Return String.Empty
            TripleDES.Key = cls_crypto.MD5Hash(key)
            TripleDES.Mode = CipherMode.ECB

            ' Converte a string encriptada para bytes e decripta 
            Dim Buffer As Byte() = Convert.FromBase64String(encryptedString)
            Return ASCIIEncoding.ASCII.GetString _
                (TripleDES.CreateDecryptor().TransformFinalBlock(Buffer, 0, Buffer.Length))

        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Application.Info.Title,
                 MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return String.Empty
        End Try
    End Function


    Public Function DecryptChave(ByVal encryptedString As String) As String

        Try
            ' Definição da chave e da cifra 
            If encryptedString = String.Empty Then Return String.Empty
            TripleDES.Key = cls_crypto.MD5Hash(key)
            TripleDES.Mode = CipherMode.ECB

            ' Converte a string encriptada para bytes e decripta
            Dim Buffer As Byte() = Convert.FromBase64String(encryptedString)
            Return ASCIIEncoding.ASCII.GetString _
                (TripleDES.CreateDecryptor().TransformFinalBlock(Buffer, 0, Buffer.Length))
        Catch ex As Exception
            MessageBox.Show("Erro na Contra_Senha, Verifique!", My.Application.Info.Title, _
                                     MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return String.Empty

        End Try
    End Function


End Class