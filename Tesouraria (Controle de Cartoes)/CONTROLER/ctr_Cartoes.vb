﻿Public Class ctr_Cartoes
    Public Sub Consulta(consultas As cls_Cartoes, form As frmCadastroCartoesLista)
        Dim con As New daoCartoes
        con.daoCartoesConsultas(consultas)
        If fs.Tables(TABELA).Rows.Count > 0 Then
            form.gcCartoes.DataSource = fs.Tables(TABELA)
        Else
            MsgBox("Não existe dados a serem exibidos!")
        End If
    End Sub


    Public Sub Incluir_Alterar(cartoes As cls_CartloesInclusao, form As frmCadastroCartoes)





        Dim VALIDA_EXECUCAO As String = "S"
        'Valida o Nome do Cartão se foi digitado
        If cartoes.nome = String.Empty Then
            MsgBox("Nome do Cartão não informado, Verifique!!", vbCritical)
            form.txNomeCartao.Focus()
            form.txNomeCartao.BackColor = Color.Yellow
            VALIDA_EXECUCAO = "N"
        End If
        'Valida se a Empresa foi Selecionada
        If cartoes.empresa = -1 Or cartoes.empresa = String.Empty Then
            MsgBox("Empresa não informada, Verifique!", vbCritical)
            form.cbEmpresas.Focus()
            form.cbEmpresas.BackColor = Color.Yellow
            VALIDA_EXECUCAO = "N"
        End If

        'Valida o tipo de Cartão
        If cartoes.tipo = -1 Or cartoes.tipo = String.Empty Then
            MsgBox("Tipo de Cartão não informado, Verifique!", vbCritical)
            form.cbTipoCartao.Focus()
            form.cbTipoCartao.BackColor = Color.Yellow
            VALIDA_EXECUCAO = "N"
        End If

        'Valida o tipo do Cartão para Gravação
        If cartoes.tipo = 0 Then
            cartoes.tipo = "C"
        ElseIf cartoes.tipo = 1 Then
            cartoes.tipo = "D"
        End If

        'Valida a taxa do Cartão
        If cartoes.Taxa <= 0 Or cartoes.Taxa = String.Empty Then
            MsgBox("Taxa de Administração do Cartão não informada ou Inválida, Verifique!", vbCritical)
            form.nudTaxa.Focus()
            form.nudTaxa.BackColor = Color.Yellow
            VALIDA_EXECUCAO = "N"
        Else
            cartoes.Taxa = Replace(cartoes.Taxa, ",", ".")
        End If

        'Valida a Exbição da Conciliadora
        If cartoes.conciliadora = -1 Or cartoes.conciliadora = String.Empty Then
            MsgBox("Informação da Conciliadora não informada ou Inválida, Verifique!", vbCritical)
            form.cbConciliadora.Focus()
            form.cbConciliadora.BackColor = Color.Yellow
            VALIDA_EXECUCAO = "N"
        End If

        'Valida se o cartão faz parte de conciliadora ou não
        If cartoes.conciliadora = 0 Then
            cartoes.conciliadora = "S"
        ElseIf cartoes.conciliadora = 1 Then
            cartoes.conciliadora = "N"
        End If

        '##########
        '########## Valida Campos de Acordo com o Tipo de Pagamento
        '##########
        If cartoes.tipoPgto = -1 Or cartoes.tipoPgto = String.Empty Then
            MsgBox("Tipo de Pagamento não Informada ou Inválida, Verifique!", vbCritical)
            form.cbTipoPgto.Focus()
            form.cbTipoPgto.BackColor = Color.Yellow
            VALIDA_EXECUCAO = "N"
        Else

            '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            '@@@ Validação A - DIA DA SEMANA
            '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            If cartoes.tipoPgto = "0" Then
                cartoes.tipoPgto = "A"
                'Valida numero de dias de vencimento
                If cartoes.diasVencimento <= 0 Then
                    MsgBox("Dias de vencimento não Infomrado, Verifique!", vbCritical)
                    form.nudDiasVcto.Focus()
                    form.nudDiasVcto.BackColor = Color.Yellow
                    VALIDA_EXECUCAO = "N"
                End If
                'Valida o dia da semana
                If cartoes.diaEnvio = String.Empty Then
                    MsgBox("Dia da semana não selecionado, Verifique!", vbCritical)
                    form.gbxSemana.Focus()
                    form.gbxSemana.BackColor = Color.Yellow
                    VALIDA_EXECUCAO = "N"
                End If

                '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                '@@@ Validação B - DIA FIXO
                '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            ElseIf cartoes.tipoPgto = "1" Then
                cartoes.tipoPgto = "B"
                If cartoes.diaFixo <= 0 Then
                    MsgBox("Dias de vencimento fixo não Infomrado, Verifique!", vbCritical)
                    form.nudDiaFixo.Focus()
                    form.nudDiaFixo.BackColor = Color.Yellow
                    VALIDA_EXECUCAO = "N"
                End If

                '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                '@@@ Validação C - DIA FIXO+PRAZO
                '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            ElseIf cartoes.tipoPgto = "2" Then
                cartoes.tipoPgto = "C"
                'Valida numero de dias de vencimento
                If cartoes.diasVencimento <= 0 Then
                    MsgBox("Dia de vencimento não Infomrado, Verifique!", vbCritical)
                    form.nudDiasVcto.Focus()
                    form.nudDiasVcto.BackColor = Color.Yellow
                    VALIDA_EXECUCAO = "N"
                End If
                'Valida se o dia Fixo foi informado.
                If cartoes.diaFixo <= 0 Then
                    MsgBox("Dias de vencimento fixo não Infomrado, Verifique!", vbCritical)
                    form.nudDiaFixo.Focus()
                    form.nudDiaFixo.BackColor = Color.Yellow
                    VALIDA_EXECUCAO = "N"
                End If

                '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                '@@@ Validação D - DIA FIXO+PRAZO+DIA SEMANA
                '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            ElseIf cartoes.tipoPgto = "3" Then

                'Valida numero de dias de vencimento
                If cartoes.diasVencimento < 0 Then
                    MsgBox("Dias de vencimento não Infomrado, Verifique!", vbCritical)
                    form.nudDiasVcto.Focus()
                    form.nudDiasVcto.BackColor = Color.Yellow
                    VALIDA_EXECUCAO = "N"
                End If
                'Valida se o dia Fixo foi informado.
                If cartoes.diaFixo <= 0 Then
                    MsgBox("Dia de vencimento fixo não Infomrado, Verifique!", vbCritical)
                    form.nudDiaFixo.Focus()
                    form.nudDiaFixo.BackColor = Color.Yellow
                    VALIDA_EXECUCAO = "N"
                End If
                'Valida o dia da semana
                If cartoes.diaEnvio = String.Empty Then
                    MsgBox("Dia da semana não selecionado, Verifique!", vbCritical)
                    form.gbxSemana.Focus()
                    form.gbxSemana.BackColor = Color.Yellow
                    VALIDA_EXECUCAO = "N"
                End If
            End If
        End If

        If VALIDA_EXECUCAO = "S" Then
            Dim con As New daoCartoes
            con.daoCartoesInsert(cartoes)
            form.txCodigo.Text = ID_INCLUIDO
            If ENTRADA_DADOS = "I" Then
                MsgBox("Cartão Incluído com Sucesso!", vbInformation)
                logDado = "Inclusão do Cartao Código: " & ID_INCLUIDO & ", Nome: " & cartoes.nome & ", Empresa: " & cartoes.empresa
                logTipo = "I"
            ElseIf ENTRADA_DADOS = "E" Then
                MsgBox("Cartão Alterado com Sucesso!", vbInformation)
                logTipo = "E"
                logDado = "Alteração do Cartao Código: " & cartoes.codigo & ", Nome: " & cartoes.nome & ", Empresa: " & cartoes.empresa & "."




            End If
            GravarLog()
        End If
    End Sub

    Public Sub EditaCartoes(cartao As cls_CartloesInclusao)
        ENTRADA_DADOS = "E"
        Dim conCartao As New daoCartoes
        Dim Form As New frmCadastroCartoes
        conCartao.consultaCartao(cartao)

        Form.txCodigo.Text = cartao.codigo
        Form.txNomeCartao.Text = cartao.nome
        Form.cbEmpresas.EditValue = cartao.empresa
        Form.nudTaxa.Value = cartao.Taxa
        Form.nudDiasVcto.Value = cartao.diasVencimento
        Form.nudDiaFixo.Value = cartao.diaFixo
        'Status do Cartao
        If cartao.status = "A" Then
            Form.rbAtivo.Checked = True
        ElseIf cartao.status = "I" Then
            Form.rbInativo.Checked = True
        End If
        'Combobox tipo de Cartão (Crédito/Débito)
        If cartao.tipo.ToString.Trim = "C" Then
            Form.cbTipoCartao.SelectedIndex = 0
        ElseIf cartao.tipo.ToString.Trim = "D" Then
            Form.cbTipoCartao.SelectedIndex = 1
        End If

        'Combobox Conciliadora (Sim/Não)
        If cartao.conciliadora.ToString.Trim = "S" Then
            Form.cbConciliadora.SelectedIndex = 0
        ElseIf cartao.conciliadora.ToString.Trim = "N" Then
            Form.cbConciliadora.SelectedIndex = 1
        End If

        'ComboBox Tipo Pagamento
        If cartao.tipoPgto.ToString.Trim = "A" Then
            Form.cbTipoPgto.SelectedIndex = 0
        ElseIf cartao.tipoPgto.ToString.Trim = "B" Then
            Form.cbTipoPgto.SelectedIndex = 1
        ElseIf cartao.tipoPgto.ToString.Trim = "C" Then
            Form.cbTipoPgto.SelectedIndex = 2
        ElseIf cartao.tipoPgto.ToString.Trim = "D" Then
            Form.cbTipoPgto.SelectedIndex = 3
        End If

        'Valida Dia da Semana
        If cartao.diaEnvio = 1 Then
            Form.rbDomingo.Checked = True
        ElseIf cartao.diaEnvio = 2 Then
            Form.rbSegunda.Checked = True
        ElseIf cartao.diaEnvio = 3 Then
            Form.rbTerca.Checked = True
        ElseIf cartao.diaEnvio = 4 Then
            Form.rbQuarta.Checked = True
        ElseIf cartao.diaEnvio = 5 Then
            Form.rbQuinta.Checked = True
        ElseIf cartao.diaEnvio = 6 Then
            Form.rbSexta.Checked = True
        ElseIf cartao.diaEnvio = 7 Then
            Form.rbSabado.Checked = True
        ElseIf cartao.diaEnvio = 8 Then
            Form.rbDiario.Checked = True
        End If

        Form.MdiParent = frmMenuPrincipal
        Form.Show()


    End Sub







End Class
