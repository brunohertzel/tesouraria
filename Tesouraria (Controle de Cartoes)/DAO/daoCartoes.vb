﻿Imports FirebirdSql.Data.FirebirdClient

Public Class daoCartoes
    Public Function daoCartoesConsultas(consultas As cls_Cartoes) As Boolean
        SQL = "SELECT * FROM CARTOES C LEFT JOIN EMPRESAS E ON E.EMPRESA_ID = C.EMPRESA_ID WHERE C.CARTAO_ID LIKE '%" & consultas.busca & "%' OR C.CARTAO_NOME LIKE '%" & consultas.busca & "%' AND C.CARTAO_STATUS='" & consultas.Status & "'"
        TABELA = "TABELA"
        CONEXAO_FIREBIRD(SQL)
        Return True

    End Function


    Public Function daoCartoesInsert(cartoes As cls_CartloesInclusao) As Boolean
        SQL = "	INSERT INTO CARTOES (EMPRESA_ID, CARTAO_NOME, CARTAO_TIPO, CARTAO_DIAS_VENCIMENTO, CARTAO_TAXA, CARTAO_DIA_ENVIO, CARTAO_DIA_VENC_FIXO, CARTAO_CONCILIADORA, CARTAO_STATUS, CARTAO_TIPO_PAGTO)
                    VALUES (@EMPRESA_ID, @CARTAO_NOME, @CARTAO_TIPO, @CARTAO_DIAS_VENCIMENTO, @CARTAO_TAXA, @CARTAO_DIA_ENVIO, @CARTAO_DIA_VENC_FIXO, @CARTAO_CONCILIADORA, @CARTAO_STATUS, @CARTAO_TIPO_PAGTO)
                    RETURNING CARTAO_ID;"
        Dim strCon As String = "User=" & FIREBIRD_USUARIO & ";Password=" & FIREBIRD_SENHA & ";Database=" & FIREBIRD_BANCO & ";DataSource=" & FIREBIRD_SERVIDOR & ";Port=" & FIREBIRD_PORTA & ";Dialect=3;charset=WIN1252;"
        Dim Conexao As New FbConnection(strCon)
        Dim Comando As New FbCommand(SQL, Conexao)
        Comando.Parameters.Add("@EMPRESA_ID", FbDbType.Integer).Value = cartoes.empresa
        Comando.Parameters.Add("@CARTAO_NOME", FbDbType.VarChar).Value = cartoes.nome
        Comando.Parameters.Add("@CARTAO_TIPO", FbDbType.Char).Value = cartoes.tipo
        Comando.Parameters.Add("@CARTAO_DIAS_VENCIMENTO", FbDbType.Char).Value = cartoes.diasVencimento
        Comando.Parameters.Add("@CARTAO_TAXA", FbDbType.Decimal).Value = Replace(cartoes.Taxa, ",", ".")
        Comando.Parameters.Add("@CARTAO_DIA_ENVIO", FbDbType.Char).Value = cartoes.diaEnvio
        Comando.Parameters.Add("@CARTAO_DIA_VENC_FIXO", FbDbType.Char).Value = cartoes.diaFixo
        Comando.Parameters.Add("@CARTAO_CONCILIADORA", FbDbType.Char).Value = cartoes.conciliadora
        Comando.Parameters.Add("@CARTAO_STATUS", FbDbType.Char).Value = cartoes.status
        Comando.Parameters.Add("@CARTAO_TIPO_PAGTO", FbDbType.Char).Value = cartoes.tipo
        Comando.Parameters.Add("CARTAO_ID", FbDbType.Integer).Direction = ParameterDirection.Output
        Conexao.Open()
        Comando.ExecuteNonQuery()
        If ENTRADA_DADOS = "I" Then
            ID_INCLUIDO = Convert.ToInt32(Comando.Parameters("CARTAO_ID").Value)
        End If
        Conexao.Close()
        Return True
    End Function

    Public Function daoCartoesUpdate(cartoes As cls_CartloesInclusao) As Boolean
        SQL = "UPDATE OR INSERT INTO CARTOES (EMPRESA_ID, CARTAO_NOME, CARTAO_TIPO, CARTAO_DIAS_VENCIMENTO, CARTAO_TAXA, CARTAO_DIA_ENVIO, CARTAO_DIA_VENC_FIXO, CARTAO_CONCILIADORA, CARTAO_STATUS, CARTAO_TIPO_PAGTO)
                    VALUES (@EMPRESA_ID, @CARTAO_NOME, @CARTAO_TIPO, @CARTAO_DIAS_VENCIMENTO, @CARTAO_TAXA, @CARTAO_DIA_ENVIO, @CARTAO_DIA_VENC_FIXO, @CARTAO_CONCILIADORA, @CARTAO_STATUS, @CARTAO_TIPO_PAGTO)
                    MATCHING EMPRESA_ID, CARTAO_ID;"
        Dim strCon As String = "User=" & FIREBIRD_USUARIO & ";Password=" & FIREBIRD_SENHA & ";Database=" & FIREBIRD_BANCO & ";DataSource=" & FIREBIRD_SERVIDOR & ";Port=" & FIREBIRD_PORTA & ";Dialect=3;charset=WIN1252;"
        Dim Conexao As New FbConnection(strCon)
        Dim Comando As New FbCommand(SQL, Conexao)
        Comando.Parameters.Add("@EMPRESA_ID", FbDbType.Integer).Value = cartoes.empresa
        Comando.Parameters.Add("@CARTAO_NOME", FbDbType.VarChar).Value = cartoes.nome
        Comando.Parameters.Add("@CARTAO_TIPO", FbDbType.Char).Value = cartoes.tipo
        Comando.Parameters.Add("@CARTAO_DIAS_VENCIMENTO", FbDbType.Char).Value = cartoes.diasVencimento
        Comando.Parameters.Add("@CARTAO_TAXA", FbDbType.Decimal).Value = Replace(cartoes.Taxa, ",", ".")
        Comando.Parameters.Add("@CARTAO_DIA_ENVIO", FbDbType.Char).Value = cartoes.diaEnvio
        Comando.Parameters.Add("@CARTAO_DIA_VENC_FIXO", FbDbType.Char).Value = cartoes.diaFixo
        Comando.Parameters.Add("@CARTAO_CONCILIADORA", FbDbType.Char).Value = cartoes.conciliadora
        Comando.Parameters.Add("@CARTAO_STATUS", FbDbType.Char).Value = cartoes.status
        Comando.Parameters.Add("@CARTAO_TIPO_PAGTO", FbDbType.Char).Value = cartoes.tipo
        Comando.Parameters.Add("CARTAO_ID", FbDbType.Integer).Value = cartoes.codigo
        Conexao.Open()
        Comando.ExecuteNonQuery()
        Conexao.Close()
        Return True
    End Function


    Public Function consultaCartao(cartao As cls_CartloesInclusao) As Boolean
        CARREGAR_INI()
        Dim strCon As String = "User=" & FIREBIRD_USUARIO & ";Password=" & FIREBIRD_SENHA & ";Database=" & FIREBIRD_BANCO & ";DataSource=" & FIREBIRD_SERVIDOR & ";Port=" & FIREBIRD_PORTA & ";Dialect=3;charset=WIN1252;"
        Dim Conexao As New FbConnection(strCon)
        Conexao.Open()
        Dim comando As FbCommand = New FbCommand("SELECT * FROM CARTOES WHERE CARTAO_ID=@CARTAO_ID", Conexao)
        comando.Parameters.Add("@CARTAO_ID", FbDbType.VarChar).Value = cartao.codigo
        Dim reader As FbDataReader = comando.ExecuteReader()
        reader.Read()
        cartao.empresa = reader("EMPRESA_ID").ToString
        cartao.codigo = reader("CARTAO_ID").ToString
        cartao.nome = reader("CARTAO_NOME").ToString
        cartao.tipo = reader("CARTAO_TIPO").ToString
        cartao.diasVencimento = reader("CARTAO_DIAS_VENCIMENTO").ToString
        cartao.Taxa = reader("CARTAO_TAXA").ToString
        cartao.diaEnvio = reader("CARTAO_DIA_ENVIO").ToString
        cartao.diaFixo = reader("CARTAO_DIA_VENC_FIXO").ToString
        cartao.conciliadora = reader("CARTAO_CONCILIADORA").ToString
        cartao.status = reader("CARTAO_STATUS").ToString
        cartao.tipoPgto = reader("CARTAO_TIPO_PAGTO").ToString
        Conexao.Close()
        Return True
    End Function


End Class
