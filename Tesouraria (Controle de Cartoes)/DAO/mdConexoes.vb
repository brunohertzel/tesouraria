﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports System.Threading.Thread
Imports System.Globalization
Imports FirebirdSql.Data.FirebirdClient
Imports System.Data.OleDb
Imports System.Collections
Imports System.Data.Odbc
Imports System.DateTime

Module mdConexoes


    Public COLOR01 As String
    Public COLOR02 As String
    Public SQL As String
    Public ENTRADA_DADOS As String
    Public CONTADOR_LOGIN As Integer

    'VARIAVEIS FIREBIRD
    Public fa As FbDataAdapter
    Public fs As DataSet

    Public fa2 As FbDataAdapter
    Public fs2 As DataSet
    'VARIAVEIS CONEXAO .INI

    Public FIREBIRD_SERVIDOR, FIREBIRD_USUARIO, FIREBIRD_SENHA, FIREBIRD_PORTA, FIREBIRD_BANCO As String
    Public FIREBIRD_SERVIDOR2, FIREBIRD_USUARIO2, FIREBIRD_SENHA2, FIREBIRD_PORTA2, FIREBIRD_BANCO2 As String
    Public BANCO_ACCESS, SEGURANCA_ACCESS, SENHA_ACCESS, ACCESS_VALIDA, BANCO_MULTIPLO As String
    Public x, CONTADOR As Long
    Public MYSQL_SERVIDOR, MYSQL_USUARIO, MYSQL_SENHA, MYSQL_PORTA, MYSQL_BANCO As String

    'Nome da Tabela -- campo tabela
    Public TABELA, CAMPO_TABELA, RETORNO, valida_acesso, TABELA1, NUM_LANC, COMPLEMENTO As String
    Public Erro_Conexao As Boolean


    '=================================================
    '====            CONEXAO FIREBIRD              ===
    '=================================================

    Sub CONEXAO_FIREBIRD(ByVal sql As String) ', ByVal conexao As String)
        ' Texto de ligação à base de dados
        'Dim myConnectionString As String = "User=SYSDBA;Password=masterkey;Database=c:\teorema\windados\TEOREMA_RASMUSSEM.FDB;DataSource=localhost;Port=3050;Dialect=3;"
        CARREGAR_INI()

        Dim myConnectionString As String = "User=" & FIREBIRD_USUARIO & ";Password=" & FIREBIRD_SENHA & ";Database=" & FIREBIRD_BANCO & ";DataSource=" & FIREBIRD_SERVIDOR & ";Port=" & FIREBIRD_PORTA & ";Dialect=3;charset=WIN1252;"

        ' Cria uma nova ligação à base de dados
        Using connection As New FbConnection(myConnectionString)

            ' Cria um novo SqlDataAdapter que servirá para atualizar o DataSet
            fa = New FbDataAdapter(sql, connection)

            ' Cria um DataSet, ou seja, uma representação em memória da informação
            fs = New DataSet

            ' Coloca a informação da tabela definida no DataSet
            Try
                fa.Fill(fs, TABELA)
            Catch erro As FbException
                MsgBox("Erro ao ler o banco de dados!" + vbCrLf + erro.ToString)
            Finally
                connection.Close()
                connection.Dispose()
            End Try
            LIMPAR_VARIAVEIS_CONEXÃO()
        End Using
    End Sub


    '#Conexao Firebird 2
    Sub CONEXAO_FIREBIRD2(ByVal sql As String) ', ByVal conexao As String)
        ' Texto de ligação à base de dados
        'Dim myConnectionString As String = "User=SYSDBA;Password=masterkey;Database=c:\teorema\windados\TEOREMA_RASMUSSEM.FDB;DataSource=localhost;Port=3050;Dialect=3;"
        CARREGAR_INI()

        Dim myConnectionString As String = "User=" & FIREBIRD_USUARIO2 & ";Password=" & FIREBIRD_SENHA2 & ";Database=" & FIREBIRD_BANCO2 & ";DataSource=" & FIREBIRD_SERVIDOR2 & ";Port=" & FIREBIRD_PORTA2 & ";Dialect=3;charset=WIN1252;"

        ' Cria uma nova ligação à base de dados
        Using connection As New FbConnection(myConnectionString)
            ' Cria um novo SqlDataAdapter que servirá para atualizar o DataSet
            fa2 = New FbDataAdapter(sql, connection)
            ' Cria um DataSet, ou seja, uma representação em memória da informação
            fs2 = New DataSet
            ' Coloca a informação da tabela definida no DataSet
            Try
                fa2.Fill(fs2, TABELA)
            Catch erro As FbException
                'MsgBox("Erro ao ler o Banco de Dados, Verifique!")
                MsgBox(Err.Description)
                Erro_Conexao = True
            Finally
                connection.Close()
                connection.Dispose()
            End Try
            LIMPAR_VARIAVEIS_CONEXÃO()
        End Using
    End Sub


    '============================================================
    '===               CONEXAO DO ARQUIVO .INI                ===
    '============================================================

    'Agora Declare as Funções das API's que ultilizaremos
    Private Declare Auto Function GetPrivateProfileString Lib "Kernel32" (ByVal lpAppName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As StringBuilder, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
    Private Declare Auto Function WritePrivateProfileString Lib "Kernel32" (ByVal lpAppName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Integer

    'Para Ler os dados do arquivo INI, usaremos a função LerArquivoDados()

    Function LeArquivoINI(ByVal nome_do_arquivo As String, ByVal sessão As String, ByVal key As String, ByVal valor_Padrão As String) As String
        Const MAX_LENGTH As Integer = 5000
        Dim string_builder As New StringBuilder(MAX_LENGTH)
        GetPrivateProfileString(sessão, key, valor_Padrão, string_builder, MAX_LENGTH, nome_do_arquivo)
        Return string_builder.ToString()
    End Function

    'Agora Usaremos a função nomeArquivoINI() para salvar o ini onde o aplicativo está.
    ' Retorna o nome do arquivo INI
    Function nomeArquivoINI() As String
        Dim nome_arquivo_ini As String = "./" 'Application.StartupPath
        Return nome_arquivo_ini & "./CONFIG.ini"
    End Function

    Sub CARREGAR_INI()
        'Ler Arquivo INI
        Dim nome_arquivo_ini As String = nomeArquivoINI()
        If Not File.Exists(nome_arquivo_ini) Then
            MsgBox("Será carregado os valores padrão do sistema.")
        End If

        'FIREBIRD
        FIREBIRD_SERVIDOR = LeArquivoINI("./CONFIG.ini", "FIREBIRD_SERVIDOR", "SERVIDOR_IP", "0")
        FIREBIRD_USUARIO = LeArquivoINI("./CONFIG.ini", "FIREBIRD_USUARIO", "USUARIO", "0")
        FIREBIRD_SENHA = LeArquivoINI("./CONFIG.ini", "FIREBIRD_SENHA", "SENHA", "0")
        FIREBIRD_PORTA = LeArquivoINI("./CONFIG.ini", "FIREBIRD_PORTA", "PORTA", "0")
        FIREBIRD_BANCO = LeArquivoINI("./CONFIG.ini", "FIREBIRD_BANCO", "BANCO", "0")

        'FIREBIRD
        FIREBIRD_SERVIDOR2 = LeArquivoINI("./CONFIG.ini", "FIREBIRD_SERVIDOR2", "SERVIDOR_IP2", "0")
        FIREBIRD_USUARIO2 = LeArquivoINI("./CONFIG.ini", "FIREBIRD_USUARIO2", "USUARIO2", "0")
        FIREBIRD_SENHA2 = LeArquivoINI("./CONFIG.ini", "FIREBIRD_SENHA2", "SENHA2", "0")
        FIREBIRD_PORTA2 = LeArquivoINI("./CONFIG.ini", "FIREBIRD_PORTA2", "PORTA2", "0")
        FIREBIRD_BANCO2 = LeArquivoINI("./CONFIG.ini", "FIREBIRD_BANCO2", "BANCO2", "0")

        'CONFIG GERAL

        COLOR01 = LeArquivoINI("./CONFIG.ini", "CONFIG", "COLOR01", "0")
        COLOR02 = LeArquivoINI("./CONFIG.ini", "CONFIG", "COLOR02", "0")
    End Sub


    Sub ESCREVER_INI_FIREBIRD()
        'Gravar Arquivo ini
        Dim nome_arquivo_ini As String = nomeArquivoINI()
        'FIREBIRD
        WritePrivateProfileString("FIREBIRD_SERVIDOR", "SERVIDOR_IP", FIREBIRD_SERVIDOR, "./CONFIG.ini")
        WritePrivateProfileString("FIREBIRD_USUARIO", "USUARIO", FIREBIRD_USUARIO, "./CONFIG.ini")
        WritePrivateProfileString("FIREBIRD_SENHA", "SENHA", FIREBIRD_SENHA, "./CONFIG.ini")
        WritePrivateProfileString("FIREBIRD_PORTA", "PORTA", FIREBIRD_PORTA, "./CONFIG.ini")
        WritePrivateProfileString("FIREBIRD_BANCO", "BANCO", FIREBIRD_BANCO, "./CONFIG.ini")
        MsgBox("Conexão com Banco de dados gravado com Sucesseo!", MsgBoxStyle.Information)
    End Sub


    Sub ESCREVER_INI_FIREBIRD2()
        'Gravar Arquivo ini
        Dim nome_arquivo_ini As String = nomeArquivoINI()
        'FIREBIRD
        WritePrivateProfileString("FIREBIRD_SERVIDOR2", "SERVIDOR_IP2", FIREBIRD_SERVIDOR2, "./CONFIG.ini")
        WritePrivateProfileString("FIREBIRD_USUARIO2", "USUARIO2", FIREBIRD_USUARIO2, "./CONFIG.ini")
        WritePrivateProfileString("FIREBIRD_SENHA2", "SENHA2", FIREBIRD_SENHA2, "./CONFIG.ini")
        WritePrivateProfileString("FIREBIRD_PORTA2", "PORTA2", FIREBIRD_PORTA2, "./CONFIG.ini")
        WritePrivateProfileString("FIREBIRD_BANCO2", "BANCO2", FIREBIRD_BANCO2, "./CONFIG.ini")
        MsgBox("Conexão com Banco de dados gravado com Sucesseo!", MsgBoxStyle.Information)
    End Sub


    Sub ESCREVER_INI_GERAL()
        'Gravar Arquivo ini
        Dim nome_arquivo_ini As String = nomeArquivoINI()
        'FIREBIRD
        WritePrivateProfileString("CONFIG", "COLOR01", COLOR01, "./CONFIG.ini")
        WritePrivateProfileString("CONFIG", "COLOR02", COLOR02, "./CONFIG.ini")
        MsgBox("Configuração Salva com Sucesso!!")
    End Sub



    Sub LIMPAR_VARIAVEIS_CONEXÃO()
        FIREBIRD_SERVIDOR = ""
        FIREBIRD_USUARIO = ""
        FIREBIRD_SENHA = ""
        FIREBIRD_PORTA = ""
        FIREBIRD_BANCO = ""
    End Sub




    Public Class MemoryManagement
        Private Declare Function SetProcessWorkingSetSize Lib "kernel32.dll" (
        ByVal process As IntPtr,
        ByVal minimumWorkingSetSize As Integer,
        ByVal maximumWorkingSetSize As Integer) As Integer

        Public Shared Sub FlushMemory()
            GC.Collect()
            GC.WaitForPendingFinalizers()
            If (Environment.OSVersion.Platform = PlatformID.Win32NT) Then
                SetProcessWorkingSetSize(Process.GetCurrentProcess().Handle, -1, -1)
            End If
        End Sub
    End Class

    Public Function ExtractNumbers(ByVal expr As String) As String
        Return String.Join(Nothing, System.Text.RegularExpressions.Regex.Split(expr, "[^\d]"))
    End Function

    Public ID_INCLUIDO As String
    Function CONEXAO_NOVA(ByVal SQL As String, ID As String)
        Dim strCon As String = "User=" & FIREBIRD_USUARIO & ";Password=" & FIREBIRD_SENHA & ";Database=" & FIREBIRD_BANCO & ";DataSource=" & FIREBIRD_SERVIDOR & ";Port=" & FIREBIRD_PORTA & ";Dialect=3;charset=WIN1252;"
        Dim Conexao As New FbConnection(strCon)
        Dim Comando As New FbCommand(SQL, Conexao)
        Conexao.Open()
        Comando.Parameters.Add(ID, FbDbType.Integer).Direction = ParameterDirection.Output
        Comando.ExecuteNonQuery()
        Conexao.Close()
        ID_INCLUIDO = Convert.ToInt32(Comando.Parameters(ID).Value)
        Return ID
    End Function






End Module
